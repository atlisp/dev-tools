(defun @dev::to-zelx ()
  (if (null (findfile "bin/LispConverter.exe"))
      (progn
	(@::down-file "bin/LispConverter.exe")
	(@::down-file "bin/DESCoder.exe")))
  (if (@::internetp)
      (while
	  (or (null (findfile "bin/LispConverter.exe"))
	      (< (vl-file-size (findfile "bin/LispConverter.exe")) 506368))
	(sleep 1)))
  (startapp "bin/LispConverter.exe"))

  
