(@::def-pkg '((:NAME . "dev-tools")
        (:FULL-NAME . "@lisp Dev-tools")
        (:AUTHOR . "VitalGG")
        (:VERSION . "1.3.86")
        (:EMAIL . "vitalgg@gmail.com")
        (:DESCRIPTION . "@lisp package develop and publish tools. It can create compile upload and publish you package.")
        (:CATEGORY . "DevelopTools")
        (:REQUIRED . "base")
        (:URL . "http://atlisp.cn")
        (:FILES "config"
            "menu"
            "git"
            "to-zelx"
            "compile-publish"
            "create-package"
            "tools-develop"
            "function-lib"
            "vscode"
            "project"
            "gen-doc"
            "support"
            "chat"
            "compat-cl"
            "remote-menu"
            "man-menus"
            "first.template"
            "first.template-chs"
            "menus.template"
            "menus.template-zh"
            "first.template-zh")))
