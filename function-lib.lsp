;; 生所可选列表
(defun @dev::fun-q-list ()
  (vl-remove-if-not
   '(lambda(x / fun-body)
      (and (wcmatch x "*:*")
	   (= 'LIST (type (setq fun-body (eval (read x)))))
	   ;;分析参数
	   (or (null (car fun-body))
	       (and (= 'list (type (car fun-body)))
		    (vl-list-length (car fun-body))
		    (apply 'and (mapcar '(lambda (y)(= 'sym (type y))) (car fun-body)))))
	   ))
   (atoms-family 1)))
(defun @dev::fun-q-class-list ()
  (list:remove-duplicates (mapcar '(lambda (x)(car (string:to-list x ":"))) (@dev::fun-q-list))))
(defun @dev::funlib-load-dev ()
  "加载开发中的 funlib,cad2020及以前的版本需要转码 utf8->cp936"
  (if (findfile (setq funlib-srcdir (strcat (@::get-config '@dev::src) "lib\\src")))
      (foreach dir (vl-remove ".." (vl-remove "." (directory funlib-srcdir nil -1)))
	       (if (findfile (strcat (system:dir funlib-srcdir) dir))
		   (foreach lspfile (directory (strcat (system:dir funlib-srcdir) dir) "*.lsp" 1)
			    (if(and
				(= 'str (type lspfile))
				(/= 126 (last (vl-string->list lspfile)))
				(/= 35 (last (vl-string->list lspfile)))
				(findfile (strcat (system:dir funlib-srcdir) dir "/" lspfile))
				(setq funsym (cadr (setq fun-expr (read (file:read-stream (strcat (system:dir funlib-srcdir) dir "/" lspfile) "utf-8"))))))
				(progn
				  (princ funsym)(princ"\n")
				  (eval (cons 'defun-q (cdr fun-expr)))
				  ))))))
  )
(defun @dev::upload-fun (fun-match / fun-lst)
  ;;(@::help (strcat "上传开源函数。"))
  (if (null (member 'pkgman @::*modules*))(@::load-module 'pkgman))
  (if (null (member 'function @::*modules*))(@::load-module 'function))
  (if (null (@::check-uploader)) (progn (@::down-uploader) (exit)))
  ;; 加载开发代码
  (@dev::funlib-load-dev)
  (if (eq 'str (type fun-match))
      (setq fun-match (read fun-match)))
  (if (eq 'sym (type fun-match))
      (setq fun-match (vl-symbol-name fun-match)))
  (if (member (car (@::string-to-list fun-match ":"))
	      (mapcar '(lambda(x) (vl-symbol-name (cdr x))) @::*alias*))
      (setq fun-match (strcat
		       (cdr (assoc (car (@::string-to-list fun-match ":"))
				    (mapcar '(lambda(x)
					       (cons (vl-symbol-name (cdr x))
						     (vl-symbol-name (car x)))
					       )
					    @::*alias*)))
		       ":" (cadr (@::string-to-list fun-match ":")))))
  (setq fun-lst (vl-remove-if-not
		 '(lambda(x)(wcmatch x fun-match))
		 (@dev::fun-q-list)))
  
  (if (= 0 (length fun-lst))
      (princ "没有匹配到可上传的函数。")
    (progn
      (princ (strcat "发现匹配的可上传函数 "
		     (itoa (length fun-lst)) " 个。\n"))
      (foreach fun% fun-lst
	       (if (@::check-userinfo)
		   (fun::upload (read fun%) "stable")
		 (fun::upload (read fun%) "release"))))))
  
(defun @dev::menu-upload-fun (/ res)
  (setq res (ui:input "上传函数"
		      '(("函数名" "" "要上传的函数，可使用 * 号匹配多个。函数名必须包含 : 号。"))))
  (@dev::upload-fun (read (cdr (assoc "函数名" res))))
  (princ)
  )

(defun @dev::menu-download-fun (/ res)
  (setq res (ui:input "下载函数"
		      '(("函数名" "" "要下载的函数，可使用 * 号匹配多个。函数名必须包含 : 号。保存到@lisp/lib 目录下。"))))
  
  (@dev::download-fun (read (cdr (assoc "函数名" res))))
  (princ)
  )

(defun @dev::download-fun (fun-match / fun-lst)
  (if (null (member 'pkgman @::*modules*))(@::load-module 'pkgman))
  (if (null (member 'function @::*modules*))(@::load-module 'function))
  (if (null (@::check-uploader)) (progn (@::down-uploader) (exit)))
  (if (eq 'str (type fun-match))
      (setq fun-match (read fun-match)))
  (if (eq 'sym (type fun-match))
      (setq fun-match (vl-symbol-name fun-match)))
  (fun::down&save fun-match))

 
(defun @dev::fun-repo ()
  (@::cmd "start" "https://gitee.com/atlisp/atlisp-lib"))

(defun @dev::make-ac-to-feishi (funname / lst-fun funname paras desc funsym feishiprefix)
  "将函数库中的函数 funname 自动完成导入飞诗编辑器"
  (if (null (member 'pkgman @::*modules*))(@::load-module 'pkgman))
  (if (null (member 'function @::*modules*))(@::load-module 'function))
  (if (null (@::check-uploader)) (progn (@::down-uploader) (exit)))
  ;; (if (eq 'str (type fun-match))
  ;;     (setq fun-match (read fun-match)))
  ;; (if (eq 'sym (type fun-match))
  ;;     (setq fun-match (vl-symbol-name fun-match)))
  (if(or (null (findfile "bin/sqlite3.exe"))
	 (null (findfile "bin/sqlite3.dll")))
      (progn
	(@::down-file "bin/sqlite3.exe")
	(@::down-file "bin/sqlite3.dll")
	(alert "正在下载 SQLite 管理程序，请稍候...")
	))
  (if (findfile (strcat (@::get-config '@dev::feishiprefix) "Data.db"))
      (progn
	(setq lst-fun (read (fun::src-code funname)))
	(setq funsym (cadr lst-fun))
	(setq paras
	      (if (member '/ (caddr lst-fun))
		  (reverse (cdr (member '/ (reverse (caddr lst-fun)))))
		(caddr lst-fun)))
	(setq desc (cadddr lst-fun))
	(if (/= 'str (type desc))
	    (setq desc ""))
	(if funsym 
	    (progn
	      (setvar "cmdecho" 0)
	      (prompt (strcat "insert AC of " (strcase (vl-symbol-name funsym) t) "\n"))
	      (@::cmd "start-bg"
		     (strcat
		      " /D " @::*prefix* "bin\\"
		      " /MIN /B  "
		      @::*prefix* "bin\\sqlite3.exe \""
		      (@::get-config '@dev::feishiprefix)"Data.db" "\" \""
		      "insert or replace into functions (Name,Syntax,Description,Style,URL,Creator,EditTime,MinArg,MaxArg) Values (\\\""
		      (strcase (vl-symbol-name funsym) t)"\\\",\\\""
		      (strcase (vl-prin1-to-string (cons funsym paras)) t)"\\\",\\\""
		      (string:subst-all " " "\n" desc)
		      "\\\",33,\\\"https://atlisp.cn\\\",\\\"@lisp\\\","
		      (car(string:to-list (@::timestamp)"."))","
		      (itoa (length  paras)) ","
		      (itoa (length paras))");\""))
	      (sleep 0.3)
	      (setvar "cmdecho" 1)))
	)
    (progn
      (alert "没有发现飞诗数据库,请设置 飞诗Lisp 路径")
      (if
	  (setq feishiprefix
		(system:get-folder "请选择 飞诗Lisp 安装路径"))
	  (@::set-config '@dev::feishiprefix (strcat feishiprefix "\\")))
      )))

(defun @dev::export-to-feishi ( / lst-funs)
  (if(or (null (findfile "bin/sqlite3.exe"))
	 (null (findfile "bin/sqlite3.dll")))
      (progn
	(@::down-file "bin/sqlite3.exe")
	(@::down-file "bin/sqlite3.dll")
	(alert "正在下载 SQLite 管理程序，请稍候...")
	(exit)
	))
  (setq fun-match (cdr (assoc "match" (ui:input "过滤" '(("match" "" "要匹配的函数名，以减少导入量。为空则全部导入。示例: string:* "))))))
  (setq lst-fun (fun::get-list))
  (if (and
       fun-match
       (/= fun-match ""))
      (setq lst-fun
	    (vl-remove-if-not
	     '(lambda (x)
		(wcmatch (vl-symbol-name x)
			 (strcase fun-match)))
	     lst-fun)))
  
  (mapcar '@dev::make-ac-to-feishi lst-fun)
  (princ))
  
(defun @dev::make-ac-file (/ str-atoms fp-notepad fp-emacs)
  "make auto complete file"
  (setq str-atoms (vl-sort
		   (mapcar
		    '(lambda (x) (strcase x T))
		    (vl-remove-if-not
		     '(lambda (x)
			(or x
			    (= 'str (type x))
			    (> (strlen x) 4)
			    (/= "C:" (substr x 1 2))
			    (/= ":" (substr x 1 2))
			    (= 'SUBR (eval (cons 'type (list (read x)))))
			    (= 'USUBR (eval (cons 'type (list (read x))))))
			    )
		     (atoms-family 1)))
		   '<))
  ;;for Emacs
  (setq fp-emacs (open (strcat @::*prefix* "autolisp-mode") "w"))
  (foreach str str-atoms
	   (write-line str fp-emacs))
  (close fp-emacs)
  
  )

(defun @dev::make-funlist-keyfile ()
  (setq fp (open (strcat @::*prefix* "atlispkeys.txt")"w"))
  (foreach key (fun::get-list)
	   (write-line (strcase(vl-symbol-name key)t) fp))
  (close fp))
;; "prin1": {
;; 		"prefix": "prin1",
;; 		"body": [
;; 			"(prin1 ${1:str})"
;; 		],
;; 		"description": "Prints an expression to the command line, or writes an expression to an open file"
;; 	}

(defun @dev::funlib-make-ac-vscode (funname / lst-fun paras desc funsym feishiprefix)
  "将函数库中的函数 funname 自动完成导入vscode"
  (if(= 'sym (type funname))(setq funname (strcase (vl-symbol-name funname) t)))
  (if (and
       (null (member (ascii ">") (vl-string->list funname)))
       (listp (setq fundesc  (read(fun::make-desc funname))))
		;;(> (strlen (nth 1 fundesc)) 2)
       )
      (progn
	;; "] " (car fundesc)"LINE_BREAK"
	;; "** " (nth 2 fundesc)"LINE_BREAK"
	;; "功能: " (nth 1 fundesc)"LINE_BREAK"
	;; "参数: " (nth 3 fundesc)"LINE_BREAK"
	;; "返回值: " (nth 4 fundesc)"LINE_BREAK"
	;; "示例: " (nth 5 fundesc)"LINE_BREAK"

	(setq funsym (read (nth 0 fundesc)))
	(setq paras (cdr (read (nth 2 fundesc))))
	(setq str-paras (@dev::vscode-handle-paras paras))
	
	(setq desc (nth 1 fundesc))
	(if (/= 'str (type desc))
	    (setq desc ""))
	(setq desc (strcat
		    desc
		    (if (and (= 'str (type (nth 4 fundesc)))
			     (/= ""  (nth 4 fundesc)))
			(strcat "\\n 返回值:" (nth 4 fundesc))
		      "")
		    (if (and (= 'str (type (nth 5 fundesc)))
			     (/= ""  (nth 5 fundesc)))
			(strcat "\\n 示例: \\n```\n" (string:subst-all "" "\n" (vl-string-trim " "(nth 5 fundesc)))"\n```")
		      "")))

	(if funsym
	    (progn
	      (write-line (strcat "\"" (strcase (vl-symbol-name funsym) t) "\": {") fp)
	      (write-line (strcat "\"prefix\" : \"" (strcase (vl-symbol-name funsym) t) "\",") fp)
	      (write-line (strcat "\"body\": [") fp)
	      (write-line (strcat "\"("
				  (strcase (vl-symbol-name funsym) t)
				  str-paras 
				  ")\"")
			  fp)
	      (write-line "],"fp)
	      (write-line (strcat
			   "\"description\":"
			   "\"" (string:subst-all "\\n " "\n"
						  (string:subst-all
						   "\\\"" "\""
						   desc)) "\"")
			  fp)
	      (write-line "}," fp)
	      )
	  )
	)))
(defun @dev::funlib-make-ac ( / lst-funs fp *error*)
  "生成 snippets 和 syntaxes"
  (defun *error* (msg)
    (if (= 'file (type fp)) (close fp))
    (@::*error* msg)
    )
  (@dev::funlib-load-dev)
  (setq lst-fun (fun::get-list))
  ;; (if (and
  ;;      fun-match
  ;;      (/= fun-match ""))
  ;;     (setq lst-fun
  ;; 	    (vl-remove-if-not
  ;; 	     '(lambda (x)
  ;; 		(wcmatch (vl-symbol-name x)
  ;; 			 (strcase fun-match)))
  ;; 	     lst-fun)))
  (if (> (@::acadver) 24)
      (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets.atlisp.json")"w" "utf8"))
    (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets.atlisp.json")"w")))
  (write-line "{" fp)
  (mapcar '@dev::funlib-make-ac-vscode lst-fun)
  (write-line "}" fp)
  (close fp)
  (princ))
