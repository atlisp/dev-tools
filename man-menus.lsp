;;; 管理远程菜单
(defun @dev:export-remote-menu ()
  (@:log "INFO" "写入远程菜单")
  (if @::*remote-menu*
      (progn
	(setq fp (open (strcat @::*prefix* "remote-menus.lsp") "w" "utf8"))
	(write-line (vl-prin1-to-string @::*remote-menu*) fp)
	(close fp))))
(defun @dev:update-remote-menu ()
  "更新本地菜单到远程，更新本地的菜单组到远程，不更新本地没有的菜单组"
  (@::load-module 'network)
  (if @::*remote-menu*
      (progn
	(foreach
	 mg% @::*menu*
	 (@:log "INFO" (strcat "handle " (car mg%)))
	 (if (setq old-mg (assoc (car mg%) @::*remote-menu*))
	     (setq @::*remote-menu*
		   (subst mg% old-mg @::*remote-menu*))
	   (setq @::*remote-menu*
		 (append @::*remote-menu*
			 (list mg%)))))
	;;排序
	(setq @::*remote-menu*
	      (vl-sort
	       @::*remote-menu*
	       '(lambda(x y)
		  (< (car x)(car y)))))
	(setq @::*remote-menu*
	      (mapcar
	       '(lambda(x)
		  (cons (car x)
			(mapcar
			 '(lambda(y)
			    (list
			     (car y)
			     (if (/= 'str (type  (cadr y)))
				 (vl-prin1-to-string (cadr y))
			       (cadr y))
			     (last y)))
			 (cdr x))))
	       @::*remote-menu*))
	(@dev:export-remote-menu)
	(@::upload "remote-menus.lsp" "." "stable")
	)
    ))
