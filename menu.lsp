(in-package :@dev)
(@::add-devmenu (_"DevelopTools") (_"Create Package") '(@::create-package))
(@::add-devmenu (_"DevelopTools") "--" "--")
(@::add-devmenu (_"DevelopTools") (_"Compile Package") '(@::compile-package))
(@::add-devmenu (_"DevelopTools") "--" "--")
(@::add-devmenu (_"DevelopTools") (_"Upload Package") '(@::upload-package))
(@::add-devmenu (_"DevelopTools") (_"Publish Package") '(@::publish-package))
(@::add-devmenu (_"DevelopTools") "--" "--")
(@::add-devmenu (_"DevelopTools") (_"Pack vlx") '(@::pack-vlx))
(@::add-devmenu (_"DevelopTools") "--" "--")
(@::add-devmenu (_"DevelopTools") (_"Get TOKEN") '(@::get-token))

(@::add-devmenu "Git" "Pull" '(@dev::clone))
(@::add-devmenu "Git" "Push" '(@dev::push))
(@::add-devmenu "Git" "Bash" '(@dev::bash))
(@::add-devmenu "Git" "Config" '(@dev::git-config))
(@::add-devmenu "Git" "Help" '(@dev::git-help))
(@::add-devmenu (_"Support") "@lisp Project" '(@dev::web-center))
(@::add-devmenu (_"Support") "Docments of Dev" '(@dev::web-doc))
(@::add-devmenu (_"Support") "Packages Center" '(@dev::web-pkgs))
(@::add-devmenu (_"Support") (_"Function lib") '(@dev::web-funlib))
(@::add-devmenu (_"Support") "VScode extension" '(@dev::install-atlisp-to-vscode))
(@::add-devmenu (_"Support") "Feishi Editor" '(@dev::open-feishi))
(@::add-devmenu (_"Support") "fas2zelx" '(@dev::to-zelx))

(@::add-devmenu "funlib" "Upload function" '(@dev::menu-upload-fun))
(@::add-devmenu "funlib" "Download function" '(@dev::menu-download-fun))
(@::add-devmenu "funlib" "Src repo" '(@dev::fun-repo))
(@::add-devmenu "funlib" "Feishi AC" '(@dev::export-to-feishi))
(@::add-devmenu "funlib" "Emacs AC" '(@dev::make-ac-file))

#-sbcl
(progn
(defun C:@@@ ()(@::dev-panel-dialog))
(defun C:@@C ()(@::compile-package))
(defun C:@@P ()(@::publish-package))
(defun C:@GP ()(@dev::push))
(defun C:@GS ()(@dev::bash))
(defun c:dxf () (@::showdxf))
)
(if (= (@::get-config '@::user) "VitalGG")
    (progn
      (@::add-devmenu (_"DevelopTools") "--" "--")
      (@::add-devmenu (_"DevelopTools") "Compile @lisp" '(@::compile-kernel))
      (@::add-devmenu (_"DevelopTools") "Upload @lisp" '(@::upload-kernel))
      (@::add-devmenu (_"DevelopTools") "Publish @lisp" '(@::publish-kernel))
      ))

(defun @::dev-panel-dialog (/ dcl-tmp dcl_fp dcl_id pkg para% run-function after-panel after-panel-cmd *error* )
  (defun *error* (msg)
    (term_dialog)
    (unload_dialog dcl_id)
    (if (= 'file (type dcl_fp))(close dcl_fp))
    (if (and (= 'str (type dcl-tmp)) (findfile dcl-tmp)) (vl-file-delete dcl-tmp))
    (@::*error* msg)
    )
  (if (= "" (@::get-config '@::user-email))
      (@::set-user-info)
    (if (= "" (@::get-config '@::user-token))
	(progn (@::load-module 'pkgman)
	       (@::get-token))
      ))
  (defun after-panel (func)
    ;; TODO: 这里加统计代码
    (if (= 'str (type func))
	(if (= 'list (type (read func)))
	    (eval (read func))
	  (if (= 'subr (type (eval(read(strcat "C:"func)))))
	      (eval(read(strcat "(C:"func")")))
	    (@::cmd func)))
      ))

  (defun run-function (func)
    (done_dialog 10)
    (setq after-panel-cmd func))
  (if @::*devmenu*
      (progn
	;; 生成 dcl 文件
	(setq dcl-tmp (vl-filename-mktemp nil nil ".dcl" ))
	(setq dcl_fp (open dcl-tmp "w"))
	(write-line (strcat "panel : dialog {"
			    "label = \"" (_"Develop Panel") "\"; "
			    ":column{label=\""(_"CurrProject")"\";"
			    ":text{value=\""(_"Name:")""
			    (if (and (/= (@::get-config '@::current-pkg) "")
				     (@::pkg (@::get-local-pkg-info (@::get-config '@::current-pkg)) ':full-name))
				(@::pkg (@::get-local-pkg-info (@::get-config '@::current-pkg)) ':full-name) "")
			    "\";}"
			    ":text{value=\""(_"Version:")""
			    (if (and (/= (@::get-config '@::current-pkg) "")
				     (@::pkg (@::get-local-pkg-info (@::get-config '@::current-pkg)) ':full-name))
				(@::pkg (@::get-local-pkg-info (@::get-config '@::current-pkg)) ':version) "")
			    "\";}"
			    "}:row {label=\"\"; ")
		    dcl_fp
		    )
	(setq i% 1)(setq bt-width 20)
	(foreach rb-menu (mapcar 'car @::*devmenu*)
		 (write-line (strcat ":radio_button{ fixed_width=true; width="
				     (itoa bt-width)
				     ";key=\"S_"(itoa i%)"\";label=\""rb-menu"\";}")
			     dcl_fp)
		 (setq i% (1+ i%))
		 )
	(write-line "}:image{ height=0.1; color=250; fixed_height=true;}:row{label=\"\";" dcl_fp)
	(setq i% 1)
	(setq max-menus
	      (apply 'max (mapcar '(lambda (x)
				    (length (vl-remove-if '(lambda (x) (= "--" (car x)))(cdr x))))
				  @::*devmenu*)))
	
	(foreach bt-menu-column @::*devmenu*
		 (write-line ":column{label=\"\";children_alignment=top;" dcl_fp)
		 (foreach bt-menu (cdr bt-menu-column)
			  ;;(setq bt-menu (vl-remove-if '(lambda (x) (= "--" (car x))) bt-menu))
			  (if (/= "--" (car bt-menu))
			      (progn
				(write-line (strcat ":button{ fixed_width=true; children_alignment=left;width="
						    (itoa bt-width)";fixed_height=true;"
						    " key=\"c_"(itoa i%)"\"; label=\""(car bt-menu)"\"; "
						    " action=\"(run-function \\\""
						    (@::string-subst "\\\\\\\"" "\"" (@::cmd2str (cadr bt-menu)))
						    "\\\")\";}")
					    dcl_fp)
				(setq i% (1+ i%)))))
		 (repeat (- max-menus (length (vl-remove-if '(lambda (x) (= "--" (car x))) (cdr bt-menu-column))))
			 (write-line (strcat ":button{ fixed_width=true; width="(itoa bt-width)"; is_enabled=false; }") dcl_fp))
		 (write-line "}" dcl_fp)
		 )
	(write-line (strcat "}" 
			    ":image{ height=0.1; color=250; fixed_height=true;}"
			    " : spacer {} ok_cancel; }")
		    dcl_fp)
	(close dcl_fp)
	
	(setq dcl_id (load_dialog dcl-tmp))
	(if (not (new_dialog "panel" dcl_id))
	    (exit))
	
	(action_tile "accept" "(done_dialog 1)")
	(start_dialog)
	(unload_dialog dcl_id)
	(vl-file-delete dcl-tmp)
	(after-panel after-panel-cmd))
      (progn 
	(alert (_"No package is installed. Please select packages that you needed to install."))
	(@::package-manager-dialog))
      ))

(if is-gcad
    (defun C:%%% ()(@::dev-panel-dialog)))
