(defpackage :@dev
  (:use :cl :autolisp :@)
  (:export :upload-fun)
  )
(in-package :@dev)
(@::define-config '@dev::editor "" "默认的代码编辑器可选 vscode lispedit notepad++ 等")
(@::define-config '@dev::git "" "Git.exe 文件路径，用于调用Git")
(@::define-config '@dev::bash "" "bash.exe 文件路径，用于调用git相关的shell脚本")
(@::define-config '@dev::git-useremail  "" "Git user Email")
(@::define-config '@dev::git-username  "" "Git user name")
(@::define-config '@::current-pkg "" (_ "Current package that you develop it."))
(@::define-config '@dev::src  (strcat (getenv "userprofile") "\\atlisp\\") "源代码仓库路径")
(@::define-config '@dev::feishiprefix  (strcat @::*prefix* "bin\\飞诗Lisp\\") "飞诗函数自动完成数据库Data.db文件路径,需以 \\ 结尾")
