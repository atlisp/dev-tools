;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; @lisp develop tools
;;; Author: VitalGG<vitalgg@gmail.com>
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

(defun @::package-src (pkg-name / kernel-src) 
  (strcat (@::get-config '@dev::src) "packages\\" pkg-name "\\"))
(defun @::compile-kernel () 
  ;; 生成版本文件。
  ;;(load "@lisp")
  (if (= 1 (getvar "lispsys")) 
    (alert 
      (strcat 
        (_ "Your CAD's version is higher. and LISPSYS=1.")
        "\n"
        (_ "And FAS files that it compiled can NOT run in LOW CAD version.\n")
        (_ "Please set LISPSYS's value as 0 or 2"))))
  (setq kernel-src (@::get-file-contents (@::path-os (strcat @::*prefix* "@lisp_u.lsp"))))
  (setq @fp (open (@::path-os(strcat @::*prefix* "kernel.lsp")) "w" "utf8"))
  (write-line 
    (vl-prin1-to-string 
      (read 
        (strcat "(progn" kernel-src ")")))
    @fp)
  (close @fp)
  (setq @fp (open (@::path-os(strcat @::*prefix* "@lisp.lsp")) "w"))
  (write-line 
    (vl-prin1-to-string 
      (read 
        (strcat "(progn" kernel-src ")")))
    @fp)
  (close @fp)
  ;; 生成 bricscad des 文件
  (if (findfile "bin/DESCoder.exe")
      (progn
	(setvar "cmdecho" 0)
	(@::cmd 
	 "start-bg"
	 (strcat 
          " /D "
          (@::path-os @::*prefix*)
          " /MIN /B  "
          "bin\\DESCoder.exe @lisp.lsp"))
	(setvar "cmdecho" 1))
      (@::down-file "bin/DESCoder.exe"))
  (if (null (= 'subr (type vlisp-compile))) (C:vlide))
  (if 
    (and 
      (= 'subr (type vlisp-compile))
      (/= "F" (load "@lisp" "F"))
      (progn (@::init) t)
      (or 
        (< (@::acadver) 24) ;; 高版本不不兼容
        (/= 1 (getvar "lispsys"))))
    (progn 
      (setq vfp (open (@::path-os (strcat @::*prefix* "version.lsp")) "w"))
      (write-line (strcat "(setq new-version \"" @::version "\")") vfp)
      (close vfp)
      (if (vlisp-compile 'lsa "@lisp.lsp") 
        (progn 
          (@::@debug "INFO" (strcat "Compile @lisp.lsp Success!"))
          T)
        (progn 
          (@::@log "ERROR" (strcat "Compile @lisp.lsp Faile!"))
          nil)))
    (progn 
      (if (= 1 (getvar "lispsys")) (alert (_ "Your CAD is higher version,And not compatible low version.")))
      (alert (_ "Please run visuallisp before the function. INPUT 'vlisp' or 'vlide' in commandline."))
      nil))
  ;; (if is-zwcad
  ;;     (compile-zelx (strcat @::*prefix* "@lisp.lsp")(strcat @::*prefix* "@lisp.zelx")))
  )

(defun @::compile-pkg (pkg-name / pkg-file% flag-compile @fp) 
  "编译应用包。返回值： 成功  T , 失败 nil"
  (if (= 1 (getvar "lispsys")) 
    (alert 
      (strcat 
        (_ "Your CAD's version is higher. and LISPSYS=1.")
        "\n"
        (_ "And FAS files that it compiled can NOT run in LOW CAD version.\n")
        (_ "Please set LISPSYS's value as 0 or 2"))))
  (if (null (= 'subr (type vlisp-compile))) (C:vlide))
  (setq flag-compile T)
  (if (null (findfile (@::path-os(strcat @::*prefix* "packages\\" pkg-name))))
    (vl-mkdir (@::path-os(strcat @::*prefix* "packages\\" pkg-name))))
  (if (findfile (@::path-os(strcat (@::package-src pkg-name) "pkg.lsp")))
    (progn 
      (@::@log "INFO" (strcat (_ "Compile ") pkg-name "..."))
      ;; 制作单个文件
      (vl-file-delete (@::path-os(strcat (@::package-path pkg-name) "pkg.lsp")))
      (setq @fp (open (@::path-os(strcat (@::package-path pkg-name) "pkg.lsp")) "w"))
      (write-line 
        (vl-prin1-to-string 
          (read (@::get-file-contents (@::path-os(strcat (@::package-src pkg-name) "pkg.lsp")))))
        @fp)
      (close @fp)

      (if (/= "failure" (load (@::path-os(strcat (@::package-path pkg-name) "pkg.lsp")) "failure")) 
        (progn 
          ;; 合并 utf8编码的 wholelsp
          (file:merge 
           (@::path-os(strcat (@::package-path pkg-name) pkg-name "-whole_u.lsp"))
            (vl-remove 
              nil
              (mapcar 
                '(lambda (x) 
                   (if (= (vl-filename-extension x) nil) 
                       (@::path-os(strcat (@::package-src pkg-name) x ".lsp"))
                       nil))
                (cdr (assoc :files (@::get-local-pkg-info pkg-name))))))
          ;; min wholelsp
          (setq @fp (open (@::path-os(strcat (@::package-path pkg-name) pkg-name "-whole.lsp")) "w"))
          (write-line 
            (vl-prin1-to-string 
              (read 
                (strcat 
                  "(progn"
                  (@::get-file-contents (@::path-os(strcat (@::package-path pkg-name) pkg-name "-whole_u.lsp")))
                  ")")))
            @fp)
          (close @fp)
          (mapcar 
            '(lambda (x) 
               (if (/= (vl-filename-extension x) nil) 
                 (progn 
                   (vl-file-delete (@::path-os(strcat (@::package-path pkg-name) x)))
                   (vl-file-copy 
                    (@::path-os(strcat (@::package-src pkg-name) x))
                    (@::path-os(strcat (@::package-path pkg-name) x))))))
            (cdr (assoc :files (@::get-local-pkg-info pkg-name))))
          (if (findfile "bin/DESCoder.exe") 
            (progn 
              (setvar "cmdecho" 0)
              (@::cmd 
                "start-bg"
                (strcat 
                  " /D "
                  (@::path-os @::*prefix*)
                  " /MIN /B  "
                  "bin\\DESCoder.exe "
                  (@::path-os (strcat (@::package-path pkg-name) pkg-name "-whole.lsp"))))
              (setvar "cmdecho" 1)))
          (if 
            (and 
              (= 'subr (type vlisp-compile))
              (or 
                (< (@::acadver) 24) ;; 高版本不不兼容
                (/= 1 (getvar "lispsys"))))
            (if (vlisp-compile 'st (@::path-os(strcat (@::package-path pkg-name) pkg-name "-whole.lsp")) )
              (progn 
                (@::@debug "INFO" (strcat (_ "Compile ") "whole Success!")))
              (progn 
                (@::@log "ERROR" (strcat (_ "Compile ") pkg-file% " Faile!"))
                (setq flag-compile nil)))
            (progn (alert "Please run visuallisp before the function. INPUT 'vlisp' or 'vlide' in commandline.") nil)))
        (setq flag-compile nil))
      (princ "----------------------\n"))
    (setq flag-compile nil))
  (if flag-compile 
    (@::prompt (_ "Compilation completed."))
    (@::prompt (_ "Compilation failed!")))
  flag-compile)

(defun @::compile-package (/ pkg-name) 
  (setq pkg-name (getstring 
                   (strcat 
                     (_ "Please input package name that you will compile")
                     " <"
                     (@::get-config '@::current-pkg)
                     ">: ")))
  (if (= "" pkg-name) 
    (setq pkg-name (@::get-config '@::current-pkg))
    (@::set-config '@::current-pkg pkg-name))

  (if 
    (and 
      (= 'str (type pkg-name))
      (< 0 (strlen pkg-name)))
    (@::compile-pkg (strcase pkg-name T))))

(defun @::compile-all () 
  "编译所有包"
  (foreach pkg-info% (mapcar 'cdr @::*local-pkgs*) 
    (setq pkg-name (strcase (cdr (assoc ':name pkg-info%)) T))
    (@::compile-pkg pkg-name)))

(defun @::upload (file pkg-name edition / curl timestamp response flag-waitting) 
  "上传包文件到网络"
  (if (null (member 'pkgman @::*modules*)) (@::load-module 'pkgman))
  (if (null (@::check-uploader)) (progn (@::down-uploader) (exit)))
  (sleep 1)
  (if (= "" (@::get-config '@::user-token)) (@::get-token))

  (@::@log "INFO" (strcat "Upload " file " ... "))
  (setq timestamp (rtos (* 86400 (getvar "date"))))
  ;;(print file)(print pkg-name )(print edition)
  (if (findfile file) 
    (progn 
      (setvar "cmdecho" 0)
      (command 
        "start-bg"
        (strcat 
          " /D "
          (@::path-os @::*prefix*)
          " /MIN /B  "
          (@::path-os @::*prefix*)
          @::*curl*
          " -A \"Atlisp-developer\" "
          " -F \"pkg-name="
          pkg-name
          "\" -F \"email="
          (@::get-config '@::user-email)
          "\" -F \"token="
          (@::get-config '@::user-token)
          "\" -F \"edition="
          edition
          "\" -F \"locale="
          (getvar "locale")
          "\" -F \"file=@"
          file
          ";filename="
          (urlencode (strcat (vl-filename-base file) (vl-filename-extension file)))
          "\" -D "
          (@::path-os @::*tmp-path*)
          "header-"
          timestamp
          "  -o "
          (@::path-os @::*tmp-path*)
          "response-"
          timestamp
          " \"http://atlisp.cn/upload\""))
      ;; 对response-timestamp 分析
      (sleep 4)
      (setvar "cmdecho" 1)
      (setq flag-waitting 0)
      (while 
        (and 
          (or 
            (null (findfile (strcat @::*tmp-path* "header-" timestamp)))
            (null (findfile (strcat @::*tmp-path* "response-" timestamp))))
          (< flag-waitting 10))
        (@::@log "WARN" (strcat "The server is not responding, please waitting ... " (itoa flag-waitting)))
        (setq flag-waitting (1+ flag-waitting))
        (sleep 3))
      (if 
        (and 
          (findfile (strcat @::*tmp-path* "header-" timestamp))
          (findfile (strcat @::*tmp-path* "response-" timestamp)))
        (cond 
          ((= "200" (@::header-code (@::parse-header (@::path-os(strcat @::*tmp-path* "header-" timestamp)))))
           (setq response (read (@::get-file-contents (@::path-os(strcat @::*tmp-path* "response-" timestamp)))))
           (princ (strcat " >> " (cdr (assoc ':msg response)) "\n"))
           T)
          ((= "100" (@::header-code (@::parse-header (@::path-os(strcat @::*tmp-path* "header-" timestamp)))))
           (sleep 3)
           (princ "...")
           (setq response (read (@::get-file-contents (@::path-os(strcat @::*tmp-path* "response-" timestamp)))))
           (princ (strcat " >> " (cdr (assoc ':msg response)) "\n"))
           T)
          (t
           (print (strcat (@::header-code (@::parse-header (@::path-os(strcat @::*tmp-path* "header-" timestamp)))) "\n"))
           nil))
        (progn 
          (@::@log "WARN" (strcat "The server is not responding, EXIT ..."))
          nil)))
    (progn 
      (@::@log "WARN" (strcat "NOT find file " file))
      nil)))

(defun @::upload-pkg (pkg-name edition / pkg-file readme-file mtime) 
  "上传包数据"
  (if (null (member 'pkgman @::*modules*)) (@::load-module 'pkgman))
  (if (null (@::check-uploader)) (progn (@::down-uploader) (exit)))
  (if (/= "stable" edition) (setq edition "files"))
  (if 
    (or 
      (= 
        (@::pkg (@::get-local-pkg-info pkg-name) ':email)
        (@::get-config '@::user-email))
      (= "vitalgg@gmail.com" (@::get-config '@::user-email)))
    ;; 如果云端有进行版本比较，云端没有的话可以直接上传。
    (if 
      (or 
        (null (@::pkg (@::get-remote-pkg-info pkg-name) ':version))
        (@::compare-version 
          (@::pkg (@::get-local-pkg-info pkg-name) ':version)
          (@::pkg (@::get-remote-pkg-info pkg-name) ':version)))
      (progn 
        (if (= "stable" edition) 
          (@::@log "INFO" (strcat (_ "Prepare to Publish package:") pkg-name))
          (@::@log "INFO" (strcat (_ "Prepare to Upload package:") pkg-name)))
        (@::upload (strcat "packages/" pkg-name "/pkg.lsp") pkg-name edition)
        ;; 上传 whole file
        (@::upload (strcat "packages/" pkg-name "/" pkg-name "-whole.fas") pkg-name edition)
        (@::upload (strcat "packages/" pkg-name "/" pkg-name "-whole.lsp") pkg-name edition)
        (if (findfile (strcat "packages/" pkg-name "/" pkg-name "-whole.des")) 
          (@::upload (strcat "packages/" pkg-name "/" pkg-name "-whole.des") pkg-name edition))

        (if (findfile (strcat "packages/" pkg-name "/" pkg-name "-whole.lsp")) 
          (@::upload (strcat "packages/" pkg-name "/" pkg-name "-whole.lsp") pkg-name edition))
        (foreach pkg-file 
          (vl-remove-if 
            '(lambda (x) (null (vl-filename-extension x)))
            (@::pkg (@::get-local-pkg-info pkg-name) ':files))
          (if (null (member (vl-filename-extension pkg-file) '(".mp4" ".ogv" ".mpg"))) 
            (@::upload 
              (strcat "packages/" pkg-name "/" pkg-file)
              pkg-name
              edition)))
        (foreach readme-file '("readme.org" "readme.txt" "readme.gif" "readme.png" "readme.mp4") 
          (if (findfile (strcat "packages/" pkg-name "/" readme-file)) 
            (progn 
              (if (null (@::check-newer-from-web (strcat "packages/" pkg-name "/" (strcase readme-file T)))) 
                (@::upload 
                  (strcat "packages/" pkg-name "/" (strcase readme-file T))
                  pkg-name
                  edition)))))
        (princ "----------------------\n")
        (@::prompt "Upload package completed")
        T)
      (progn 
        (@::@log "WARN" (strcat (_ "Package ") pkg-name " is old version in local, DENY"))
        (@::prompt "Upload package fail")
        nil))
    (progn 
      (@::@log "WARN" (strcat "Your are not " pkg-name "'s author,DENY"))
      nil)))

(defun @::upload-package (/ pkg-name) 
  (setq pkg-name (getstring 
                   (strcat 
                     (_ "Please input package name that you will UPLOAD")
                     " <"
                     (@::get-config '@::current-pkg)
                     ">: ")))
  (if (= "" pkg-name) 
    (setq pkg-name (@::get-config '@::current-pkg))
    (@::set-config '@::current-pkg pkg-name))
  (princ "Starting ...\n")
  (if 
    (and 
      (= 'str (type pkg-name))
      (< 0 (strlen pkg-name)))
    (if (@::compile-pkg (strcase pkg-name T)) 
      (@::upload-pkg (strcase pkg-name T) "release")
      (@::@log "ERROR" "Compilation failed!"))
    (@::@log "ERROR" "Package is not exist")))
(defun @::publish-package (/ pkg-name) 
  ;; (@::help "上传 和 发布 应用。")
  (setq pkg-name (getstring 
                   (strcat 
                     (_ "Please input package name that you will PUBLISH")
                     " <"
                     (@::get-config '@::current-pkg)
                     ">: ")))
  (prin1)
  (if (= "" pkg-name) 
    (setq pkg-name (@::get-config '@::current-pkg))
    (@::set-config '@::current-pkg pkg-name))
  (princ "Starting ...\n")
  (if 
    (and 
      (= 'str (type pkg-name))
      (< 0 (strlen pkg-name)))
    (if 
      (and 
        (@::compile-pkg (strcase pkg-name T))
        (string-equal @::*edition* "stable"))
      (progn 
        (@::upload-pkg (strcase pkg-name T) "release")
        (@::upload-pkg (strcase pkg-name T) "stable")
	(@::@get "https://atlisp.cn/flush-data")
	)
      (@::@log "ERROR" "Compile failure!"))
    (@::@log "ERROR" "Package is not exist")))

(defun @::upload-kernel () 
  (textpage)
  (if (null (member 'pkgman @::*modules*)) (@::load-module 'pkgman))
  (if (@::compile-kernel) 
    (progn 
      (@::@log "INFO" "Upload @lisp kernel ...\n")
      (@::upload "@lisp.fas" "." "files")
      (@::upload "version.lsp" "." "files")
      (@::upload "@lisp.dcl" "." "files"))
    (progn 
      (@::@log "ERROR" "Compile kernel fail.\n")
      nil)))
(defun @::publish-kernel () 
  ;;(@::help "上传 kernel 的 release 版本和 stable 版本")
  (if (null (member 'pkgman @::*modules*)) (@::load-module 'pkgman))
  (if 
    (and 
      (@::compile-kernel)
      (string-equal @::*edition* "stable"))
    (progn 
      (@::@log "INFO" "Prepare to PUBLISH @lisp kernel.\n")
      (@::upload "@lisp.fas" "." "files")
      (@::upload "version.lsp" "." "files")
      (@::upload "@lisp.dcl" "." "files")
      (@::upload "@lisp.fas" "." "stable")
      (@::upload "version.lsp" "." "stable")
      (@::upload "@lisp.dcl" "." "stable"))
    (progn 
      (@::@log "ERROR" "Compile kernel failure.\n")
      nil)))

