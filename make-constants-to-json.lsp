(defun @dev::make-constants ()
  "生成CAD对象系统"
  (setq constants (mapcar 
		   '(lambda (x)(cons x (eval(read x))))
		   (vl-remove-if-not 
		    '(lambda (x) 
		      (and 
		       (wcmatch x "AC*")
		       (wcmatch x "~ACET*")
		       (numberp (eval (read x)))
		       )
		      )
		    (atoms-family 1))))
  
  (setq fpjson (open "C:\\Users\\vitalgg\\atlisp/vscode/snippets/ac-constants.json" "a"))
  (write-line "{" fpjson)
  (foreach
   constant% constants
   (write-line (strcat
		"\""(car constant%)"\":"
		(itoa (cdr constant%))
		",")
	       fpjson)
   )
  (write-line "}" fpjson)
  (close fpjson))
