(defun @dev::check-git (/ gitexe)
  (if (null (findfile (@::get-config '@dev::git)))
      (if (and (ui:confirm (_"Not found Git, to config path?"))
	       (setq gitexe (getfiled "Select git.exe"  @::*prefix* "exe" 0))
	       (= "git"(vl-filename-base gitexe)))
	  (@::set-config '@dev::git gitexe)
	(progn
	  (if (ui:confirm '((_"Git tool is not found. Please download and install it manually.")
			    (_"Git tools are required to develop @ lisp.")))
	      (progn 
		(@::cmd "start" "https://pan.baidu.com/s/1rpfm3pLYIU3wS1V4gXLN0w?pwd=zgl5")
		nil))))
    (if (= "" (@::get-config '@dev::git-useremail))
	(@dev::git-config)
      T)
    ))
(defun @dev::git-config()
  (setq res (ui:input "Config Git User"
		      (list
		       (list "Email" (@::get-config '@dev::git-useremail) "Gitee Email")
		       (list "Name" (@::get-config '@dev::git-username)  "Gitee Username"))))
  (if (/= "" (vl-string-trim " "(cdr (assoc "Email" res))))
      (progn
	(@::set-config '@dev::git-username (cdr (assoc "Name" res)))
	(@::set-config '@dev::git-useremail (cdr (assoc "Email" res)))
	(if (findfile (@::get-config '@dev::git))
	    (progn
	      (setvar "cmdecho" 0)
	      (@::cmd "start-bg"
		     (strcat " /D \""
			     (@::get-config '@dev::src)
			     "\" /MIN /B "
			     (@dev::shell) " "
			     "~/atlisp/config-git.sh" " \""(cdr (assoc "Name" res)) "\"  \"" (cdr (assoc "Email" res))"\""
			     ))
	      (setvar "cmdecho" 1)
	)))))
  
(defun @dev::clone ()
  (if (@dev::check-git)
      (if (null (findfile(strcat (getenv "userprofile")"\\atlisp")))
	  (progn
	    (setvar "cmdecho" 0)
	    (@::cmd "start"
		   (strcat " /D \"" (getenv "userprofile")
			   "\"  "
			   (@::get-config '@dev::git) " clone https://gitee.com/atlisp/atlisp.git "
			   ))
	    (setvar "cmdecho" 1))
	(@dev::pull)
	)
    ))
(defun @dev::pull ()
  (if (@dev::check-git)
      (progn
	 (setvar "cmdecho" 0)
	 (@::cmd "start-bg"
		(strcat " /D \""
			(@::get-config '@dev::src)
			"\" /MIN /B "
			(@dev::shell)" "
			"~/atlisp/pull-all.sh"
			))
	 (setvar "cmdecho" 1)
	 )))

(defun @dev::push (/ str-commit)
  (if (and (@dev::check-git)
	   (/= "" (@::get-config '@::current-pkg))
	   (setq str-commit
		 (cdr (assoc "Commit"
			     (ui:input
			      (strcat "Commit for " (@::get-config '@::current-pkg))
			      '(("Commit" "" "Information of this push"))))))
	   )
      (progn
	(if (= "" (vl-string-trim " " str-commit))
	    (setq str-commit "No infomation"))
	(setvar "cmdecho" 0)
	(@::cmd "start-bg"
	       (strcat " /D \""
		       (if (/= "" (@::get-config '@::current-pkg))
			   (@::package-src (@::get-config '@::current-pkg))
			 (@::get-config '@dev::src))
		       "\" /MIN /B  "
		       (@dev::shell)" "
		       "~/atlisp/push-pkg.sh" " "
		       (if (/= "" (@::get-config '@::current-pkg))
			   (@::get-config '@::current-pkg)
			 "./")
		       " \""  str-commit "\"")
	       )
	(setvar "cmdecho" 1)
	)
    (@::log "WARNING" "Need git tools or nocurrent package ")
    )
  (princ)
  )
   
(defun @dev::bash ()
  (if (null (findfile(strcat (getenv "userprofile")"\\atlisp")))
      (@dev::clone)
    (if (@dev::check-git)
	(progn
	  (setvar "cmdecho" 0)
	  (@::cmd "start-bg"
		 (strcat " /D \""
			 (if (/= "" (@::get-config '@::current-pkg))
			     (@::package-src (@::get-config '@::current-pkg))
			 (@::get-config '@dev::src))
			 "\" /MIN /B  "
			 (@dev::shell))
		 )
	  (setvar "cmdecho" 1)
	  ))))
(defun @dev::shell ()
  (if (null (findfile(strcat (getenv "userprofile")"\\atlisp")))
      (@dev::clone)
    (if (@dev::check-git)
	(progn
	  (if (findfile (strcat (vl-filename-directory (@::get-config '@dev::git))"\\..\\git-bash.exe"))
	      (strcat (vl-filename-directory (@::get-config '@dev::git))"\\..\\git-bash.exe")
	    (strcat (vl-filename-directory (@::get-config '@dev::git))"\\bash.exe")
	    )))))

(defun @dev::git-pull()
  "pull repo"
  
  )
	       
(defun @dev::git-push()
  "push repo"

  )
	       
(defun @dev::git-add()
  "add repo"

  )
(defun @dev::git-commit (str)
  "Commit"

  )

(defun @dev::git-help ()
  (@::cmd "start" "https://gitee.com/atlisp/atlisp-docs/blob/main/Git%E4%BD%BF%E7%94%A8%E6%8C%87%E5%8D%97.org"))
