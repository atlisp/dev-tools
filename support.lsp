(defun @dev::open-feishi ()
  (if (findfile (strcat (@::get-config '@dev::feishiprefix)
			"LispEdit.exe"))
      (startapp (strcat (@::get-config '@dev::feishiprefix)
			"LispEdit.exe"))
    (alert "Not found LispEdit.exe")))
(defun @dev::open-file (file-lsp / editor)
  (if (null (findfile (@::get-config '@dev::editor)))
      (progn
	(setq editor (getfiled "Select Editor" "" "exe" 8))
	(if editor
	    (@::set-config '@dev::editor editor)))
    (startapp (strcat
	       (@::get-config '@dev::editor)
	       " " file-lsp))))
   
