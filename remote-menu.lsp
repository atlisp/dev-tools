(defun @dev::remote-menu ()
  "编辑远程菜单，因为要用 clisp进行维护，所以代码项不能是列表(冒号前的包名与clisp不兼容).需要转为字符串类型"
  (setq @::*remote-menu*
	(mapcar
	 '(lambda(x)
	   (cons (car x)
	    (mapcar
	     '(lambda(y)
	       (list
		(car y)
		(if (/= 'str (type  (cadr y)))
		    (vl-prin1-to-string (cadr y))
		    (cadr y))
		(last y)))
	     (cdr x))))
	 @::*remote-menu*))
  (setq fp (open "D:/remote-menus.lsp" "w" "utf8"))
  (write-line (vl-prin1-to-string @::*remote-menu*) fp)
  (close fp))
  
  
