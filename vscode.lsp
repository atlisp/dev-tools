(defun @dev::install-atlisp-to-vscode ()
  (@::help "安装 @lisp 扩展到 VScode.")
  ;;(vl-registry-descendents "HKEY_CLASSES_ROOT\\vscode")
  (setvar "cmdecho" 0)
  (if (vl-registry-read "HKEY_CLASSES_ROOT\\vscode")
      (@::cmd "start" "vscode:extension/VitalGG.atlisp-funlib")
    (progn
      (alert (@::speak "在您的系统中没有发现 VScode,请先下载安装。"))
      (@::cmd "start" "https://code.visualstudio.com/")
      ))
  (setvar "cmdecho" 1)
  )
(defun @dev::vscode-handle-paras (paras / str-paras i)
    (setq str-paras "")
    (setq i 0)
    (foreach para paras
	     (setq str-paras
		   (strcat str-paras " "
			   (cond
			    ((= 'str (type para))
			     (if (= "|"(substr para 1 1))
				 (strcat "\"${" (itoa (setq i (1+ i)))
					 para "}\"")
			       (strcat "\"${" (itoa (setq i (1+ i)))":"
				       para "}\""))
			     )
			    ((atom para)
			     (if (= "|" (substr (vl-symbol-name para) 1 1))
				 (strcat "${"(itoa (setq i (1+ i)))
					 (strcase (vl-symbol-name para) t)
					 "}"
					 )
			       (strcat "${"(itoa (setq i (1+ i)))":"
					 (strcase (vl-symbol-name para) t)
					 "}"
					 )
			       ))
			    ((and (listp para)
				  (= 'quote (car para)))
			     (strcat "'${"(itoa (setq i (1+ i)))":"
				     (strcase (vl-prin1-to-string(cadr para))t)
				     "}"
				     ))
			    (t
			     (vl-prin1-to-string para)
			     ))
			   )))
    str-paras)
(defun @dev::make-ac(/ fp *error* prefix page str-pre
				str-pre-title qqq
				)
  (defun *error*(msg)
    (if fp (close fp))
    (if fi (close fi))
    (princ msg))
  (setq file-src (getfiled "要生成 vscode AC 的源文件" (strcat (@::get-config '@dev::src)  "docs/aibot/") "org" 8))
  ;; (setq qprefix (getstring t (@::speak "为了预防问题重复，请输入问题前缀（一般为空，但有例外,如DXF）:")))
  ;; (setq prefix (vl-string-trim " " (getstring t (@::speak "请输入显示前缀:"))))
  (setq prefix "")(setq qprefix "")
  (setq fi (open file-src "r"))
  (if(> (@::acadver) 24)
      (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets." (vl-filename-base file-src) ".json")"w" "utf8"))
    (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets." (vl-filename-base file-src) ".json")"w")))
  ;; (write-line chat-title fp)
  (write-line "{" fp)
  (setq str-pre-title "")
  (setq body nil)
  (setq str-desc "") (setq page 0)

  (while (setq str-line (read-line fi))
    (cond
     
     ((= "#+"  (substr str-line 1 2))
      )
     ((= "* " (substr str-line 1 2))
      (cond
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     (/= "" str-body)
	     (listp body)
	     (> (length body) 0)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
	(write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\"("
			    (string:subst-all
			     "\\\"" "\""
			     (strcat
			      (if (= (strcase (vl-symbol-name (car body)) t)
				     (strcase (vl-string-left-trim "函数" (car qqq)) t))
				  (vl-string-left-trim "函数" (car qqq))
				(strcase (vl-symbol-name (car body)) t)
				)
			      str-paras))
			    ")\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}," fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	)
       ;; 无代码的关键字
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     ;;(/= "" str-body)
	     (null body)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
	(write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\""
			    (string:subst-all
			     "\\\"" "\""
			     (vl-string-left-trim "函数" (car qqq))
			     )
			    "\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}," fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	)
       (t
	(setq str-pre-title str-line))
       )
      )
     ((= "** " (substr str-line 1 3))
      (setq str-body (substr str-line 4))
      (princ str-line)
      (if (listp (read str-body))
	  (setq body (read str-body))
	))
     (t
      (if (/= "" (vl-string-trim " " str-line))
	  (if (= "" str-desc)
	      (setq str-desc str-line)
	    (setq str-desc (strcat str-desc "\\n " str-line) )))
      ))
    )
  ;; 写最后一个
  (cond
   ((and (/= "" str-pre-title)
	   (/= "" str-desc)
	   (/= "" str-desc)
	   (/= "" str-body)
	   (listp body)
	   (> (length body) 0)
	   )
      (progn 
	(setq qqq (@dev::parse-question str-pre-title))
    	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
		
	(write-line (strcat  "\"" (car qqq)"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (car qqq) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\"("
			    (string:subst-all
			     "\\\"" "\""
			     (strcat
			      (if (= (strcase (vl-symbol-name (car body)) t)
				     (strcase (vl-string-left-trim "函数" (car qqq)) t))
				  (vl-string-left-trim "函数" (car qqq))
				(strcase (vl-symbol-name (car body)) t)
				)
			      str-paras))
			    ")\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}" fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	))
   ;; 无代码的关键字
   ((and (/= "" str-pre-title)
	 (/= "" str-desc)
	 ;;(/= "" str-body)
	 (null body)
	 )
    (princ str-pre-title)
    (setq qqq (@dev::parse-question str-pre-title))
    (setq paras (cdr body))
    (setq str-paras (@dev::vscode-handle-paras paras))
    (write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
    (write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
    (write-line (strcat "\"body\": [") fp)
    (write-line (strcat "\""
			(string:subst-all
			 "\\\"" "\""
			 (vl-string-left-trim "函数" (car qqq))
			 )
			"\"")
		fp)
    (write-line "],"fp)
    (write-line (strcat
		 "\"description\":"
		 "\""
		 (string:subst-all
		  " " "\n" (string:subst-all
			    "\\\"" "\""str-desc))
		 "\"")
		fp)
    (write-line "}," fp)
    (setq str-pre-title str-line)
    (setq str-body "")(setq body nil)
    (setq str-desc "")
    ))
  (write-line "}" fp)
  (close fp)
  (close fi)
  ;;(@::speak "已生成聊天数据库，请到微信对话开放平台导入。")
  )
(defun @dev::make-ac-dcl (/ fp *error* prefix page str-pre
				       str-pre-title qqq)
  (defun *error*(msg)
    (if fp (close fp))
    (if fi (close fi))
    (princ msg))
  (setq file-src (getfiled "要生成 vscode AC 的源文件" (strcat @::*prefix* ".cache/") "org" 8))
  (setq prefix "")(setq qprefix "")
  (setq fi (open file-src "r"))
  (if(> (@::acadver) 24)
      (setq fp (open (strcat @::*prefix* ".cache/snippets." (vl-filename-base file-src) ".json")"w" "utf8"))
    (setq fp (open (strcat @::*prefix* ".cache/snippets." (vl-filename-base file-src) ".json")"w")))
  ;; (write-line chat-title fp)
  (write-line "{" fp)
  (setq str-pre-title "")
  (setq body nil)
  (setq str-desc "") (setq page 0)
  (while (setq str-line (read-line fi))
    (cond
     ((= "#+"  (substr str-line 1 2))
      )
     ((= "* " (substr str-line 1 2))
      (cond
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
		
	(write-line (strcat  "\"" (car qqq)"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (car qqq) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\": "
			    (string:subst-all
			     "\\\"" "\""
			     (strcat ": " (car qqq)
				     "{ $1 }"))
			    "\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}," fp)
	(setq str-pre-title str-line)
	(setq body nil)
	(setq str-desc "")
	)
       (t
	(setq str-pre-title str-line))
       )
      )
     (t
      (if (/= "" (vl-string-trim " " str-line))
	  (if (= "" str-desc)
	      (setq str-desc str-line)
	    (setq str-desc (strcat str-desc "\\n " str-line) )))
      ))
    )
  ;; 写最后一个
  (if (and (/= "" str-pre-title)
	   (/= "" str-desc)
	   )
      (progn 
	(setq qqq (@dev::parse-question str-pre-title))
    		
	(write-line (strcat  "\"" (strcat (car qqq)"\": {")) fp)
	(write-line (strcat "\"prefix\" : \"" (car qqq) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\": "(car qqq)
			    "{ $1 }"
			    "\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}" fp)
	(setq str-pre-title str-line)
	(setq str-body "")
	(setq body nil)
	(setq str-desc "")

 
	))
  (write-line "}" fp)
  (close fp)
  (close fi)
  )
(defun @dev::make-ac-dcl-att (/ fp *error* prefix page str-pre
				       str-pre-title qqq)
  (defun *error*(msg)
    (if fp (close fp))
    (if fi (close fi))
    (princ msg))
  (setq file-src (getfiled "要生成 vscode AC 的源文件" (strcat @::*prefix* ".cache/") "org" 8))
  (setq prefix "")(setq qprefix "")
  (setq fi (open file-src "r"))
  (if(> (@::acadver) 24)
      (setq fp (open (strcat @::*prefix* ".cache/snippets." (vl-filename-base file-src) ".json")"w" "utf8"))
    (setq fp (open (strcat @::*prefix* ".cache/snippets." (vl-filename-base file-src) ".json")"w")))
  ;; (write-line chat-title fp)
  (write-line "{" fp)
  (setq str-pre-title "")
  (setq body nil)
  (setq str-desc "") (setq page 0)
  (while (setq str-line (read-line fi))
    (cond
     ((= "#+"  (substr str-line 1 2))
      )
     ((= "* " (substr str-line 1 2))
      (cond
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
	
	(write-line (strcat  "\"" (car qqq)"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (car qqq) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\""
			    (string:subst-all
			     "\\\"" "\""
			     (strcat (car qqq)
				     "=$1;"))
			    "\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}," fp)
	(setq str-pre-title str-line)
	(setq body nil)
	(setq str-desc "")
	)
       (t
	(setq str-pre-title str-line))
       )
      )
     (t
      (if (/= "" (vl-string-trim " " str-line))
	  (if (= "" str-desc)
	      (setq str-desc str-line)
	    (setq str-desc (strcat str-desc "\\n " str-line) )))
      ))
    )
  ;; 写最后一个
  (if (and (/= "" str-pre-title)
	   (/= "" str-desc)
	  	   )
      (progn 
	(setq qqq (@dev::parse-question str-pre-title))
    		
	(write-line (strcat  "\"" (strcat (car qqq)"\": {")) fp)
	(write-line (strcat "\"prefix\" : \"" (car qqq) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\""(car qqq)
			    "=$1;"
			    "\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}" fp)
	(setq str-pre-title str-line)
	(setq str-body "")
	(setq body nil)
	(setq str-desc "")

 
	))
  (write-line "}" fp)
  (close fp)
  (close fi)
  ;;(@::speak "已生成聊天数据库，请到微信对话开放平台导入。")
  )
(defun @dev::make-ac-dxf (/ fp *error* prefix page str-pre
				       str-pre-title qqq)
  (defun *error*(msg)
    (if fp (close fp))
    (if fi (close fi))
    (princ msg))
  (setq file-src (strcat (@::get-config '@dev::src) "docs/aibot/dxf.org"))
  ;; (setq qprefix (getstring t (@::speak "为了预防问题重复，请输入问题前缀（一般为空，但有例外,如DXF）:")))
  ;; (setq prefix (vl-string-trim " " (getstring t (@::speak "请输入显示前缀:"))))
  (setq prefix "")(setq qprefix "")
  (setq fi (open file-src "r"))
  
  (if(> (@::acadver) 24)
      (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets." (vl-filename-base file-src) ".json")"w" "utf8"))
    (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets." (vl-filename-base file-src) ".json")"w")))		
  ;; (write-line chat-title fp)
  (write-line "{" fp)
  (setq str-pre-title "")
  (setq body nil)
  (setq str-desc "") (setq page 0)
  (while (setq str-line (read-line fi))
    (cond
     ((= "#+"  (substr str-line 1 2))
      )
     ((= "* " (substr str-line 1 2))
      (cond
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
	(setq paras (cdr body))

	(foreach
	 qqq% qqq
	 (setq qqq% (vl-string-trim " " qqq%))
	 (write-line (strcat  "\"dxf" qqq%"\": {") fp)
	 (write-line (strcat "\"prefix\" : \"dxf" qqq% "\",") fp)
	 (write-line (strcat "\"body\": [") fp)
	 (if(= 'int (type (read qqq%)))
	     (progn
	       (setq dxfn (read qqq%))
	       (cond
		((or (<= 0 dxfn 9)
		     (<= 100 dxfn 102)
		     (<= 300 dxfn 309)
		     (<= 390 dxfn 399)
		     (<= 410 dxfn 419)
		     (<= 430 dxfn 439)
		     (<= 470 dxfn 479)
		     (<= 1000 dxfn 1009))
		 (write-line (strcat "\"("
				     (string:subst-all
				      "\\\"" "\""
				      (strcat qqq%
					      " . \"$1\""))
				     ")\"")
			     fp))
		((or (= 105 dxfn)
		     (<= 310 dxfn 319)
		     (<= 320 dxfn 329)
		     (<= 330 dxfn 339)
		     (<= 390 dxfn 399)
		     (<= 480 dxfn 481)
		     )
		 (write-line (strcat "\"("
				     (string:subst-all
				      "\\\"" "\""
				      (strcat qqq%
					      " . \"${1:HEX}\""))
				     ")\"")
			     fp))
		((or (<= 10 dxfn 39)
		     (= 1010 dxfn)
		     )
		 (write-line (strcat "\"(cons "
				     (string:subst-all
				      "\\\"" "\""
				      (strcat qqq%
					      " ${1:pt}"))
				     ")\"")
			     fp))
		((< dxfn -1)
		 (write-line (strcat "\"(cons "
				     (string:subst-all
				      "\\\"" "\""
				      (strcat qqq%
					      " \"$1\""))
				     ")\"")
			     fp))
		((= dxfn -1)
		 (write-line (strcat "\"(cons "
				     (string:subst-all
				      "\\\"" "\""
				      (strcat qqq%
					      " ${1:ename}"))
				     ")\"")
			     fp))
		
		(t
		 (write-line (strcat "\"(cons "
				     (string:subst-all
				      "\\\"" "\""
				      (strcat qqq%
					      " ${1:num}"))
				     ")\"")
			     fp))))
	   (write-line (strcat "\"("
			       (string:subst-all
				"\\\"" "\""
				(strcat "0 . \""
					qqq%"\""
					))
				")\"")
		       fp))
	 
	 (write-line "],"fp)
	 (write-line (strcat
		      "\"description\":"
		      "\""
		      (string:subst-all
		       " " "\n" (string:subst-all
				 "\\\"" "\""str-desc))
		      "\"")
		     fp)
	 (write-line "}," fp))
	(setq str-pre-title str-line)
	(setq body nil)
	(setq str-desc "")
	)
       (t
	(setq str-pre-title str-line))
       )
      )
     (t
      (if (/= "" (vl-string-trim " " str-line))
	  (if (= "" str-desc)
	      (setq str-desc str-line)
	    (setq str-desc (strcat str-desc "\\n " str-line) )))
      ))
    )
  ;; 写最后一个
  (if (and (/= "" str-pre-title)
	   (/= "" str-desc)
	   )
      (progn 
	(setq qqq (@dev::parse-question str-pre-title))
    		
	(write-line (strcat  "\"" (strcat (car qqq)"\": {")) fp)
	(write-line (strcat "\"prefix\" : \"" (car qqq) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\"("(car qqq)
			    " . $1);"
			    "\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}" fp)
	(setq str-pre-title str-line)
	(setq str-body "")
	(setq body nil)
	(setq str-desc "")

 
	))
  (write-line "}" fp)
  (close fp)
  (close fi)
  ;;(@::speak "已生成聊天数据库，请到微信对话开放平台导入。")
  )

(defun @dev::make-ac-vl ()
  (setq fp (open (strcat (@::get-config '@dev::src) "vscode/syntaxes/vla-property.tmlang.json") "w" "utf8"))
  
  (setq fi (open (strcat (@::get-config '@dev::src) "docs/aibot/vlaobj-property.org") "r"))
  (setq str "")
  (while (setq ln (read-line fi))
    (if (= "* " (substr ln 1 2))
	(setq str (strcat str "|" (vl-string-trim "* " ln)))))
  (close fi)
  
  (write-line (strcat "{\n\"name\": \"property\",") fp)
  (write-line (strcat "\"match\": \"(?<=\\\\(|\\\\[|'|\\\\s|^)(?<=vla-get-|vla-put-)(?i:" str ")(?=\\\\]|\\\\s|\\\\)|\\\\()\"\n}") fp)
  (close fp)

  (setq fp (open (strcat (@::get-config '@dev::src) "vscode/syntaxes/vla-method.tmlang.json") "w" "utf8"))
  
  (setq fi (open (strcat (@::get-config '@dev::src) "docs/aibot/vlaobj-method.org") "r"))
  (setq str "")
  (while (setq ln (read-line fi))
    (if (= "* " (substr ln 1 2))
	(setq str (strcat str "|" (vl-string-trim "* " ln)))))
  (close fi)
  
  (write-line (strcat "{\n\"name\": \"method\",") fp)
  (write-line (strcat "\"match\": \"(?<=\\\\(|\\\\[|'|\\\\s|^)(?<=vla-)(?i:"str ")(?=\\\\]|\\\\s|\\\\)|\\\\()\"\n}") fp)
  (close fp)
  )
(defun @dev::make-ac-sysvar (/ fp *error* prefix page str-pre
				str-pre-title qqq
				)
  (defun *error*(msg)
    (if fp (close fp))
    (if fi (close fi))
    (princ msg))
  (setq file-src (getfiled "要生成 vscode AC 的源文件" (strcat (@::get-config '@dev::src)  "docs/aibot/") "org" 8))
  ;; (setq qprefix (getstring t (@::speak "为了预防问题重复，请输入问题前缀（一般为空，但有例外,如DXF）:")))
  ;; (setq prefix (vl-string-trim " " (getstring t (@::speak "请输入显示前缀:"))))
  (setq prefix "")(setq qprefix "")
  (setq fi (open file-src "r"))
  (if(> (@::acadver) 24)
      (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets." (vl-filename-base file-src) ".json")"w" "utf8"))
    (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets." (vl-filename-base file-src) ".json")"w")))
  ;; (write-line chat-title fp)
  (write-line "{" fp)
  (setq str-pre-title "")
  (setq body nil)
  (setq str-desc "") (setq page 0)

  (while (setq str-line (read-line fi))
    (cond
     
     ((= "#+"  (substr str-line 1 2))
      )
     ((= "* " (substr str-line 1 2))
      (cond
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     (/= "" str-body)
	     (listp body)
	     (> (length body) 0)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
	(write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\"("
			    (string:subst-all
			     "\\\"" "\""
			     (strcat
			      (if (= (strcase (vl-symbol-name (car body)) t)
				     (strcase (vl-string-left-trim "函数" (car qqq)) t))
				  (vl-string-left-trim "函数" (car qqq))
				(strcase (vl-symbol-name (car body)) t)
				)
			      str-paras))
			    ")\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}," fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	)
       ;; 无代码的关键字
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     ;;(/= "" str-body)
	     (null body)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
	(write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\"\\\""
			    (string:subst-all
			     "\\\"" "\""
			     (vl-string-left-trim "函数" (car qqq))
			     )
			    "\\\"\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}," fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	)
       (t
	(setq str-pre-title str-line))
       )
      )
     ((= "** " (substr str-line 1 3))
      (setq str-body (substr str-line 4))
      (princ str-line)
      (if (listp (read str-body))
	  (setq body (read str-body))
	))
     (t
      (if (/= "" (vl-string-trim " " str-line))
	  (if (= "" str-desc)
	      (setq str-desc (strcat "系统变量: " str-line))
	    (setq str-desc (strcat str-desc "\\n " str-line) )))
      ))
    )
  ;; 写最后一个
  (cond
   ((and (/= "" str-pre-title)
	   (/= "" str-desc)
	   (/= "" str-desc)
	   (/= "" str-body)
	   (listp body)
	   (> (length body) 0)
	   )
      (progn 
	(setq qqq (@dev::parse-question str-pre-title))
    	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
		
	(write-line (strcat  "\"" (car qqq)"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (car qqq) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\"("
			    (string:subst-all
			     "\\\"" "\""
			     (strcat
			      (if (= (strcase (vl-symbol-name (car body)) t)
				     (strcase (vl-string-left-trim "函数" (car qqq)) t))
				  (vl-string-left-trim "函数" (car qqq))
				(strcase (vl-symbol-name (car body)) t)
				)
			      str-paras))
			    ")\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}" fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	))
   ;; 无代码的关键字
   ((and (/= "" str-pre-title)
	 (/= "" str-desc)
	 ;;(/= "" str-body)
	 (null body)
	 )
    (princ str-pre-title)
    (setq qqq (@dev::parse-question str-pre-title))
    (setq paras (cdr body))
    (setq str-paras (@dev::vscode-handle-paras paras))
    (write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
    (write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
    (write-line (strcat "\"body\": [") fp)
    (write-line (strcat "\"\\\""
			(string:subst-all
			 "\\\"" "\""
			 (vl-string-left-trim "函数" (car qqq))
			 )
			"\\\"\"")
		fp)
    (write-line "],"fp)
    (write-line (strcat
		 "\"description\":"
		 "\""
		 (string:subst-all
		  " " "\n" (string:subst-all
			    "\\\"" "\""str-desc))
		 "\"")
		fp)
    (write-line "}," fp)
    (setq str-pre-title str-line)
    (setq str-body "")(setq body nil)
    (setq str-desc "")
    ))
  (write-line "}" fp)
  (close fp)
  (close fi)
  ;;(@::speak "已生成聊天数据库，请到微信对话开放平台导入。")
  )
(defun @dev::make-ac-funlib ()
  (@dev::funlib-make-ac))
(defun @dev::make-ac-doslib (/ fp *error* prefix page str-pre
				str-pre-title qqq
				)
  (defun *error*(msg)
    (if fp (close fp))
    (if fi (close fi))
    (princ msg))
  (setq file-src (getfiled "要生成 vscode AC 的源文件" (strcat (@::get-config '@dev::src)  "docs/aibot/") "org" 8))
  ;; (setq qprefix (getstring t (@::speak "为了预防问题重复，请输入问题前缀（一般为空，但有例外,如DXF）:")))
  ;; (setq prefix (vl-string-trim " " (getstring t (@::speak "请输入显示前缀:"))))
  (setq prefix "")(setq qprefix "")
  (setq fi (open file-src "r"))
  (if(> (@::acadver) 24)
      (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets." (vl-filename-base file-src) ".json")"w" "utf8"))
    (setq fp (open (strcat (@::get-config '@dev::src) "vscode/snippets/snippets." (vl-filename-base file-src) ".json")"w")))
  ;; (write-line chat-title fp)
  (write-line "{" fp)
  (setq str-pre-title "")
  (setq body nil)
  (setq str-desc "") (setq page 0)

  (while (setq str-line (read-line fi))
    (cond
     
     ((= "#+"  (substr str-line 1 2))
      )
     ((= "* " (substr str-line 1 2))
      (cond
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     (/= "" str-body)
	     (listp body)
	     (> (length body) 0)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
	(write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\"("
			    (string:subst-all
			     "\\\"" "\""
			     (strcat
			      (if (= (strcase (vl-symbol-name (car body)) t)
				     (strcase (vl-string-left-trim "函数" (car qqq)) t))
				  (vl-string-left-trim "函数" (car qqq))
				(strcase (vl-symbol-name (car body)) t)
				)
			      str-paras))
			    ")\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}," fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	)
       ;; 无代码的关键字
       ((and (/= "" str-pre-title)
	     (/= "" str-desc)
	     ;;(/= "" str-body)
	     (null body)
	     )
	(princ str-pre-title)
	(setq qqq (@dev::parse-question str-pre-title))
	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
	(write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\""
			    (string:subst-all
			     "\\\"" "\""
			     (vl-string-left-trim "函数" (car qqq))
			     )
			    "\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}," fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	)
       (t
	(setq str-pre-title str-line))
       )
      )
     ((= "** " (substr str-line 1 3))
      (setq str-body (substr str-line 4))
      (princ str-line)
      (if (listp (read str-body))
	  (setq body (read str-body))
	))
     (t
      (if (/= "" (vl-string-trim " " str-line))
	  (if (= "" str-desc)
	      (setq str-desc str-line)
	    (setq str-desc (strcat str-desc "\\n " str-line) )))
      ))
    )
  ;; 写最后一个
  (cond
   ((and (/= "" str-pre-title)
	   (/= "" str-desc)
	   (/= "" str-desc)
	   (/= "" str-body)
	   (listp body)
	   (> (length body) 0)
	   )
      (progn 
	(setq qqq (@dev::parse-question str-pre-title))
    	(setq paras (cdr body))
	(setq str-paras (@dev::vscode-handle-paras paras))
		
	(write-line (strcat  "\"" (car qqq)"\": {") fp)
	(write-line (strcat "\"prefix\" : \"" (car qqq) "\",") fp)
	(write-line (strcat "\"body\": [") fp)
	(write-line (strcat "\"("
			    (string:subst-all
			     "\\\"" "\""
			     (strcat
			      (if (= (strcase (vl-symbol-name (car body)) t)
				     (strcase (vl-string-left-trim "函数" (car qqq)) t))
				  (vl-string-left-trim "函数" (car qqq))
				(strcase (vl-symbol-name (car body)) t)
				)
			      str-paras))
			    ")\"")
		    fp)
	(write-line "],"fp)
	(write-line (strcat
		     "\"description\":"
		     "\""
		     (string:subst-all
		      " " "\n" (string:subst-all
				"\\\"" "\""str-desc))
		     "\"")
		    fp)
	(write-line "}" fp)
	(setq str-pre-title str-line)
	(setq str-body "")(setq body nil)
	(setq str-desc "")
	))
   ;; 无代码的关键字
   ((and (/= "" str-pre-title)
	 (/= "" str-desc)
	 ;;(/= "" str-body)
	 (null body)
	 )
    (princ str-pre-title)
    (setq qqq (@dev::parse-question str-pre-title))
    (setq paras (cdr body))
    (setq str-paras (@dev::vscode-handle-paras paras))
    (write-line (strcat  "\"" (vl-string-left-trim "函数" (car qqq))"\": {") fp)
    (write-line (strcat "\"prefix\" : \"" (vl-string-left-trim "函数" (car qqq)) "\",") fp)
    (write-line (strcat "\"body\": [") fp)
    (write-line (strcat "\""
			(string:subst-all
			 "\\\"" "\""
			 (vl-string-left-trim "函数" (car qqq))
			 )
			"\"")
		fp)
    (write-line "],"fp)
    (write-line (strcat
		 "\"description\":"
		 "\""
		 (string:subst-all
		  " " "\n" (string:subst-all
			    "\\\"" "\""str-desc))
		 "\"")
		fp)
    (write-line "}," fp)
    (setq str-pre-title str-line)
    (setq str-body "")(setq body nil)
    (setq str-desc "")
    ))
  (write-line "}" fp)
  (close fp)
  (close fi)
  ;;(@::speak "已生成聊天数据库，请到微信对话开放平台导入。")
  )
