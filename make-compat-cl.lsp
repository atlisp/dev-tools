(setq chat-title "分类(必填),问题(必填),相似问题(选填-多个用##分隔),反例问题(选填-多个用##分隔),机器人回答(必填-多个用##分隔),是否全部回复(选填-默认FALSE),是否停用(选填-默认FALSE)")
;; <a href="weixin://bizmsgmenu?msgmenucontent=ssname&msgmenuid=ssname">ssname</a>
(defun @dev:parse-question (str-q / res q% questions)
	(setq questions (string:to-list (vl-string-left-trim "* " str-q) ","))
	(setq res nil)
	(foreach question% questions
		 (if (member (ascii "-") (cdr (vl-string->list question%)))
		     (progn
		       (setq q% (string:to-lst question% "-"))
		       (if (and
			    (string:intp (car q%))
			    (string:intp (cadr q%))
			    (< (atoi (car q%))
			       (atoi (cadr q%))))
			   (progn (setq qs%  (list (car q%)))
				  (repeat (- (atoi (cadr q%)) (atoi (car q%)))
					  (setq qs% (cons (itoa (1+ (atoi (car qs%)))) qs%)))
				  (setq res (append res (reverse qs%))))
			 (setq res (append res (list question%)))))
		   (setq res (append res (list question%)))))
	res)
(defun @dev:make-chat-funlib(/ fp *error* lst-fun)
  (defun *error*(msg)
    (if fp (close fp))
    (princ msg))
  (@dev:funlib-load-dev)
  (setq fp (open (strcat @:*prefix* ".cache/chat-funlib.csv")"w"))
  (write-line chat-title fp)
  (setq lst-fun (fun:get-list))
  (foreach fun lst-fun
	   (setq fun (vl-symbol-name fun))
	   (if (and
		(setq  funname (strcase fun t))
		(null (member (ascii ">") (vl-string->list funname)))
		(listp (setq fundesc  (read(fun:make-desc funname))))
		;;(> (strlen (nth 1 fundesc)) 2)
		)
	       (write-line
		(strcat 
		 "@lisp函数库,"(string:subst-all "_" ","funname)","
		 (if (> (strlen (nth 1 fundesc)) 2)
		     (vl-string-trim "？?。，." 
				     (string:subst-all "##" ","(substr (car (string:to-list (nth 1 fundesc) "\n")) 1 20 )))
		   "")
		 ",,"
		 (string:subst-all "LINE_BREAK" "\n"
		 (string:subst-all "，" ","
				   (strcat
				    "*** ["
				    (@dev:make-bizmsgmenu"=@lisp函数=")
				    "] " (car fundesc)"LINE_BREAK"
				    "** " (nth 2 fundesc)"LINE_BREAK"
				    "功能: " (nth 1 fundesc)"LINE_BREAK"
				    "参数: " (nth 3 fundesc)"LINE_BREAK"
				    "返回值: " (nth 4 fundesc)"LINE_BREAK"
				    "示例: " (nth 5 fundesc)"LINE_BREAK"
				    (if(and (vl-position (read fun) lst-fun)
					    (nth (1+ (vl-position (read fun) lst-fun)) lst-fun))
					(strcat 
					 "LINE_BREAK=> "
					 (@dev:make-bizmsgmenu
					  (strcat "="
						  (strcase
						   (vl-symbol-name 
						    (nth (1+ (vl-position (read fun) lst-fun)) lst-fun))
						   t)
						  "=")))
				      "")
				    )))
		 ",false,false")
		fp))
	   )
  (close fp)
  )
(defun @dev:make-compat-cl (/ fp *error* prefix page str-pre
			    str-pre-title qqq)
  (defun *error*(msg)
    (if fp (close fp))
    (if fi (close fi))
    (princ msg))
  (setq file-src (getfiled "要生成 compat-cl  数据库的源文件" (strcat (@:get-config '@dev:src) "docs/aibot/") "org" 8))
  ;; (setq qprefix (getstring t (@:speak "为了预防问题重复，请输入问题前缀（一般为空，但有例外,如DXF）:")))
  ;; (setq prefix (vl-string-trim " " (getstring t (@:speak "请输入显示前缀:"))))
  (setq prefix "")(setq qprefix "")
  (setq fi (open file-src "r"))
  (setq fp (open (strcat @:*prefix* ".cache/" (vl-filename-base file-src) ".lisp")"w"))
  ;; (write-line chat-title fp)
  (setq str-pre-title "")
  (setq str-pre "") (setq page 0)
  (while (setq str-line (read-line fi))
    (cond
     ((= "#+"  (substr str-line 1 2))
       ;; (cond
       ;; ((vl-string-search "#+prefix:" str-line)
       ;; 	(setq prefix (vl-string-trim " " (cadr (string:to-list str-line ":"))))
       ;; 	(if (/= "" prefix)
       ;; 	    (setq prefix
       ;; 		  (strcat "["
       ;; 			  (@dev:make-bizmsgmenu (strcat "="prefix"=")) "]: ")))
       ;; 	)
       ;; ((vl-string-search "#+qprefix:" str-line)
       ;; 	(setq qprefix (vl-string-trim " " (cadr (string:to-list str-line ":"))))))
      )
     ((= "* " (substr str-line 1 2))
      (cond
       ((and (/= "" str-pre-title)
	     (or (/= "" str-pre)
		 (> page 0))
	     )
	;; 分析前组数据并生成QA
	;; title , - 写其它问法
	(setq qqq (@dev:parse-question str-pre-title))
	(setq answer-as-question
	      (car (vl-remove-if '(lambda(x) (= "*" (substr x 1 1)))
				 (string:to-list str-pre "LINE_BREAK"))))
	
	(if (> (length (string:s2l-ansi str-pre)) 500)
	    (@:log "WARN" (strcat str-pre-title " is longer, " (itoa (length (string:s2l-ansi str-pre))))))
	(write-line
	 (strcat 
	  (vl-filename-base file-src)","(strcat qprefix
			(car qqq)(if (> page 0)(strcat"-续"(itoa page))""))
	  ","
	  (if (and
	       (= page 0)
	       (< (apply 'max (vl-string->list (car qqq))) 127)
	       )
	      (if (cdr qqq)
		  (strcat 
		   (string:from-list 
		    (mapcar '(lambda(x)
			       (strcat qprefix
				       (vl-string-trim " " x)))
			    (cdr qqq))
		    "##")
		   "##" answer-as-question)
		answer-as-question)
	    "")
	  ",,"
	  ;;(string:subst-all "\\\"" "\"" str-pre)
	  (if (/= "" str-pre)
	      (string:subst-all "，" "," str-pre)
	    "...")
	  "LINE_BREAKLINE_BREAK => "
	  (@dev:make-bizmsgmenu(strcat "=" qprefix(car (@dev:parse-question str-line))"="))
	  ",false,false")
	 fp)
	(setq str-pre-title str-line)
	(setq str-pre (strcat "*** " prefix  (car (@dev:parse-question str-line))))
	)
       (t
	(setq str-pre-title str-line))
       )
      (setq page 0)
      )
     (t
      (setq str-pre (if (/= "" str-pre)
			(strcat str-pre "LINE_BREAK"(@dev:make-bizmsgmenu  str-line))
		      (@dev:make-bizmsgmenu str-line)
		      ))
      ;; 长度分隔
      (if (> (length (string:s2l-ansi str-pre)) 500)
	  (progn
	    (setq qqq (@dev:parse-question str-pre-title))
	    (write-line
	     (strcat 
	      (vl-filename-base file-src)","
	      
	      (strcat qprefix (car qqq)
		      (if (> page 0)(strcat"-续"(itoa page))""))
	      ","
	      (if (and
		   (cdr qqq)
		   (= page 0))
		  (string:from-list 
		   (mapcar '(lambda(x)
			      (strcat qprefix
				      (vl-string-trim " " x)))
			   (cdr qqq))
		   "##")
		"")
	      ",,"
	      ;;(string:subst-all "\\\"" "\"" str-pre)
	      (string:subst-all "，" "," str-pre)
	      "LINE_BREAKLINE_BREAK => "
	      (@dev:make-bizmsgmenu(strcat "=" qprefix(car qqq)
					   (strcat"-续"(itoa (setq page (1+ page))))"="))
	      ",false,false")
	     fp)
	    (setq str-pre "")
	    )
	)
      )
     ))
  ;; 写最后一个
  (if (and (/= "" str-pre-title)
	   (/= "" str-pre))
      ;; 分析前组数据并生成QA
      ;; title , - 写其它问法
      (progn
	(setq qqq (@dev:parse-question str-pre-title))
	(write-line
	 (strcat 
	  (vl-filename-base file-src)","(strcat qprefix
			(car qqq)(if (> page 0)(strcat"-续"(itoa page))""))
	  ","
	  (if (and
	       (cdr qqq)
	       (= page 0))
	      (string:from-list 
	       (mapcar '(lambda(x)
			  (strcat qprefix
				  (vl-string-trim " " x)))
		       (cdr qqq))
	       "##")
	    "")
	  ",,"
	  ;;(string:subst-all "\\\"" "\"" str-pre)
	  (string:subst-all "，" "," str-pre)
	  ",false,false")
	 fp)
	;; (setq str-pre-title str-line)
	(setq str-pre "")
	))
  (close fp)
  (close fi)
  (@:speak "已生成 compat-cl 。")
  )

