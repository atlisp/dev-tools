(defun @::string-replace-in-file ( newstr oldstr lspfile / vf zf text)
  "替换文件中的字符串。"
  (if (findfile (strcat lspfile ".tmp"))
      (vl-file-delete (strcat lspfile ".tmp")))
  (vl-file-copy lspfile (strcat lspfile ".tmp"))
  (setq vf (open (strcat lspfile ".tmp") "r"))
  (setq zf (open lspfile "w"))
  (while (setq text (read-line vf))
    (write-line (@::string-subst newstr oldstr text) zf)
    );while
  (close vf)
  (close zf)
  (if (findfile (strcat lspfile ".tmp"))
      (vl-file-delete (strcat lspfile ".tmp")))
  )
(if (null (findfile (strcat (@::package-path "dev-tools") "first.template")))
    (@::down-pkg-file  (@::uri) "dev-tools/first.template" "stable"))
(if (and (member @::locale '("chs" "zh"))
	 (null (findfile (strcat "packages/dev-tools/first.template-" @::locale))))
    (@::down-pkg-file  (@::uri) (strcat "dev-tools/first.template-" @::locale)  "stable"))

(if (null (findfile "packages/dev-tools/menus.template"))
    (@::down-pkg-file  (@::uri) "dev-tools/menus.template" "stable"))
(if (and (member @::locale '("chs" "zh"))
	 (null (findfile (strcat "packages/dev-tools/menus.template-" @::locale))))
    (@::down-pkg-file  (@::uri) (strcat "dev-tools/menus.template-" @::locale)  "stable"))
(defun @::create-pkg (pkg-name pkg-fullname pkg-category pkg-desc multifiles vlxname vlxcommand / pkg-lsp-fp lsp-file-fp)
  ;; vlxname 对中文名字的处理
  (if (null (@::check-userinfo))
      (progn
	(alert (_"User infomation is invalid."))(exit)))

  (if (or (null pkg-category)
	  (= "" pkg-category))
      (setq pkg-category "Common"))
  ;;(@dev::clone)
  
  (vl-mkdir (@::package-src pkg-name))
  (vl-mkdir (@::package-path pkg-name))
  
  (setq pkg-lsp-fp (open (strcat (@::package-src pkg-name) "pkg.lsp") "w"))
  (write-line (strcat "(@::def-pkg "
		      "'((:name . \"" pkg-name "\")"
		      "  (:full-name . \""  pkg-fullname "\")"
		      "  (:author . \""  (@::get-config '@::user)  "\")"
		      "  (:email . \"" (@::get-config '@::user-email) "\")"
		      "  (:version . \"0.0.1\")"
		      "  (:locale . \"" @::locale "\")"
		      "  (:category . \"" pkg-category "\")"
		      "  (:required . \"base\")"
		      "  (:opensource . 0) "
		      "  (:description . \"" pkg-desc "\")"
		      "  (:url . \"http://atlisp.cn\")"
		      "  (:files . (\"" pkg-name "\"")
	      pkg-lsp-fp)
  (if (and vlxname (findfile vlxname))
      (progn
	(vl-file-copy (findfile vlxname) (strcat (@::package-src pkg-name) (vl-filename-base vlxname) ".vlx"))
	(write-line (strcat "  \"" (vl-filename-base vlxname) ".vlx" "\"") pkg-lsp-fp)))
  (if multifiles
      (write-line (strcat "  \"menus\"") pkg-lsp-fp))
  (write-line (strcat "              ;; you can add file name that you will included in the package.") pkg-lsp-fp)
  (write-line (strcat "              ;; lsp file must not input extand name.") pkg-lsp-fp)
  (write-line (strcat "              ))))") pkg-lsp-fp)
  (close pkg-lsp-fp)
  (vl-file-delete (strcat (@::package-path pkg-name) "pkg.lsp"))
  (vl-file-copy (strcat (@::package-src pkg-name) "pkg.lsp")
		(strcat (@::package-path pkg-name) "pkg.lsp"))

  ;;创建lisp文件
  (if (and vlxname (findfile vlxname))
      (progn ;;create lsp file
	(alert "vlx")

	(setq pkglsp (open  (strcat (@::package-src pkg-name) pkg-name ".lsp") "w"))
	(write-line (strcat "(@::add-menu \"Plugins\" \"" pkg-fullname "\" " "\"(" pkg-name":loader)\")") pkglsp)
	(write-line (strcat "(defun " pkg-name ":loader ()") pkglsp)
	(write-line (strcat "(load-vlx  \"" pkg-name "/" (vl-filename-base vlxname) ".vlx\" C:" vlxcommand "))") pkglsp)
	(close pkglsp)
	)
    (progn
      (if (member @::locale '("chs" "zh"))
	  (vl-file-copy (findfile (strcat "packages/dev-tools/first.template-" @::locale))
			(strcat (@::package-src pkg-name) pkg-name ".lsp"))
	(vl-file-copy (findfile "packages/dev-tools/first.template")
		      (strcat (@::package-src pkg-name) pkg-name ".lsp")))
      (@::string-replace-in-file pkg-name "{pkg}" (strcat (@::package-src pkg-name) pkg-name ".lsp"))
      (@::string-replace-in-file pkg-fullname "{pkg-full-name}" (strcat (@::package-src pkg-name) pkg-name ".lsp"))
      ))
  (if (member @::locale '("chs" "zh"))
      (vl-file-copy (findfile (strcat "packages/dev-tools/menus.template-" @::locale))
		    (strcat (@::package-src pkg-name) "menus.lsp"))
    (vl-file-copy (findfile "packages/dev-tools/menus.template")
		  (strcat (@::package-src pkg-name) "menus.lsp")))
  (@::string-replace-in-file pkg-name "{pkg}" (strcat (@::package-src pkg-name) "menus.lsp"))
  (@::string-replace-in-file pkg-fullname "{pkg-full-name}" (strcat (@::package-src pkg-name) "menus.lsp"))
  ;;  (if multifiles (
  (alert (strcat (_"Your package is created.")"\n"
		 (_"Your package is located in path:")
		 "\n\n " (@::package-src pkg-name)  "\n\n"
		 (_"Please develop it use your tools.")))
  (@::set-config '@::current-pkg pkg-name)
  )
;; (defun @dev::check-pkg-name (name)
;;   (if (null (@::check-uploader)) (progn (@::down-uploader)(exit)))
;;   (sleep 1)
;;   (@::log "INFO" (strcat "check package name  ... "))
;;   (while (null (@::check-userinfo)) (alert (_"User infomation is invalid.")))
;;   (setq timestamp  (rtos (* 86400 (getvar "date"))))
;;   (setvar "cmdecho" 0)
;;   (command "start-bg"
;; 	   (strcat " /D " @::*prefix*
;; 		   " /MIN /B  "
;; 		   @::*prefix* @::*curl*
;; 		   "   -F \"timestamp=" timestamp
;; 		   "\" -F \"pkg-name=" name
;; 		   "\" -F \"locale=" @::locale
;; 		   "\" -D " @::*tmp-path* "header-" timestamp 
;; 		   "  -o  " @::*tmp-path* "response-" timestamp 
;; 		   " \"http://atlisp.cn/existp\""  ))
;;   ;; 对response-timestamp 分析
;;   (setvar "cmdecho" 1)
;;   (sleep 1)
;;   (if (findfile (strcat @::*tmp-path* "response-" timestamp))
;;       (progn
;; 	(setq header (@::parse-header (strcat @::*tmp-path* "header-" timestamp)))
;; 	(if (= 200 (atoi (@::header-code header)))
;; 	    (progn
;; 	      ;; insert img into dwg
;; 	      (setq response (read (@::get-file-contents  (strcat @::*tmp-path* "response-" timestamp))))
;; 	      (cdr (assoc ':result response))
;; 	      )
;; 	  (progn
;; 	    (alert (strcat (_"read network fail.")))
;; 	    ))
;; 	)))

(defun check-package-name (pkg-name)
  "check package name is valid."
  (if (member pkg-name
	      (mapcar '(lambda (x) (cdr (assoc ':name x)))
		      (vl-remove nil (append @::*pkgs*
					     @::*pkgs-release*
					     (mapcar 'cdr @::*local-pkgs*)))))
      (progn (alert (strcat "The package name '" pkg-name "' is duplicate.")) nil)
    (if (apply 'or (mapcar '(lambda (x) (vl-string-search x pkg-name)) '(":" "\\" "/" "|" "?" "*")))
	(progn (alert (strcat "The package name '" pkg-name "' include invalid char.")) nil)
      T)
    ))
(defun @::create-package (/ dcl_id dcl-tmp pkg-name create-package callback-create-package *error*)
  ;;(@::help (strcat "创建应用包,需输入用于区别其它应用包的英文名称。中文名称及其它项可在生成的 pkg.lsp 文件中修改。\n"))
  (defun *error* (msg)
    (unload_dialog dcl_id)
    (vl-file-delete dcl-tmp)
    (@::*error* msg)
    )
  (if (null (@::check-userinfo)) (progn(alert (_"User infomation is invalid."))(exit)))
  (if (/= 'subr (type @::package-update))(progn (@::load-module 'pkgman) (sleep 1)))
  (@::load-module 'pkgman)
  (@::package-update)
  (if (null (vl-file-directory-p (@::get-config '@dev::src)))
      (@dev::clone))
  (defun callback-create-package (/ dcl-tmp dcl_fp dcl_id )
    (setq pkg-name (strcase (get_tile "name") T))
    ;; 检查英文名
    (if (and (apply 'and (mapcar '(lambda (x) (null (member (ascii x)(vl-string->list pkg-name))))
				 '(" " "\\" "/" "*")))
	     (check-package-name pkg-name))
	(if (and (= 'str (type pkg-name))
		 (< 0 (strlen pkg-name)))
	    (progn
	      (@::create-pkg pkg-name
			    (get_tile "fullname")
			    (if (> (atoi (get_tile "category")) 0)
				(nth (1- (atoi (get_tile "category"))) @::*pkgs-category*)
			      "Uncategorized")
			    (get_tile "description")
			    (if (= "1" (get_tile "multifiles"))
				T
			      nil)
			    (get_tile "vlxname")
			    (get_tile "vlxcommand")
			    )
	      ))
      (alert (stract "The package name '" pkg-name "' have invalid word."))
      ))
  
  "创建包信息"
  (setq dcl-tmp (vl-filename-mktemp nil nil ".dcl" ))
  (setq dcl_fp (open dcl-tmp "w"))
  (foreach line% 
	   (list
	    (strcat "createPackage : dialog {label=\"" (_"Create Package") "\";" )
	    (strcat ":column {:row {:edit_box {label=\""(_"PKG Name")
		    ":\";key=\"name\";width=36;fixed_width=true;edit_width=20;alignment=right;}")
	    
	    (strcat ":text {label=\""(_"English name") "\";key =\"name_desc\";width=50; } }")
	    (strcat  ":row {: edit_box {label=\""(_"Full Name")":\";" )
	    "key=\"fullname\";width=36;fixed_width=true;edit_width=20;alignment=right; }"
	    (strcat ":text_part {label=\""
		    (_"The name in your local language.")
		    "\";key =\"fullname_desc\";width=50;}}")
	    (strcat ":row {:popup_list{label=\"" (_"Category")
		    ":\";key=\"category\";width=36;fixed_width=true;edit_width=20;alignment=right;}")
	    (strcat  ": text_part {	label=\""(_"The package in category.")
		     "\";key =\"category-desc\";width=50;}}")
	    (strcat ": row {: edit_box {label=\"" (_"Description")
		    ":\";key=\"description\";width=95;fixed_width=true;edit_width=80;alignment=right;}}" )
	    (strcat ": row {: toggle {label=\""
		    (_"Multi Files:include menus,hotkey etc.")
		    "\";key=\"multifiles\";width=95;fixed_width=true;edit_width=80;alignment=right;}}")
	    "} ok_cancel;}")
	   (write-line line% dcl_fp))
  (close dcl_fp)
  (setq dcl_id (load_dialog dcl-tmp))
  (if (not (new_dialog "createPackage" dcl_id))
      (exit))
  (start_list "category")
  (add_list "Uncategorized")
  (mapcar 'add_list @::*pkgs-category*)
  (end_list)
  (action_tile "accept" "(callback-create-package)(done_dialog)")
  (start_dialog)
  (unload_dialog dcl_id)
  (vl-file-delete dcl-tmp)
  (princ)
  )

;; 其它 vlx 封装。

(defun @::pack-vlx (/ dcl_id pkg-name create-package callback-create-package Select-vlx)
  ;;(@::help (strcat "创建应用包,需输入用于区别其它应用包的英文名称。中文名称及其它项可在生成的 pkg.lsp 文件中修改。\n"))
  (if(null (@::check-userinfo))(progn (alert (_"User infomation is invalid."))(exit)))
  (@::package-update)
  (defun check-package-name (pkg-name)
    "check package name is valid."
    (if (member pkg-name
		(mapcar '(lambda (x) (cdr (assoc ':name x)))
			(vl-remove nil (append @::*pkgs*
					       @::*pkgs-release*
					       (mapcar 'cdr @::*local-pkgs*)))))
	(progn (alert (strcat "The package name '" pkg-name "' is duplicate.")) nil)
      (if (apply 'or (mapcar '(lambda (x) (vl-string-search x pkg-name)) '(":" "\\" "/" "|" "?" "*")))
	  (progn (alert (strcat "The package name '" pkg-name "' include invalid char.")) nil)
	T)
      ))
  (defun callback-create-package (/ dcl-tmp dcl_fp dcl_id )
    (setq pkg-name (strcase (get_tile "name") T))
    (if (check-package-name pkg-name)
	(if (and (= 'str (type pkg-name))
		 (< 0 (strlen pkg-name)))
	    (progn
	      (@::create-pkg pkg-name
			    (get_tile "fullname")
			    (if (> (atoi (get_tile "category")) 0)
				(nth (1- (atoi (get_tile "category"))) @::*pkgs-category*)
			      "Uncategorized")
			    (get_tile "description")
			    (if (= "1" (get_tile "multifiles"))
				T
			      nil)
			    (get_tile "vlxname")
			    (get_tile "vlxcommand")
			    )
	      )
	  (alert (stract "The package name '" pkg-name "' have invalid word."))
	  )))
  (defun Select-vlx ()
    (set_tile "vlxname" (getfiled "Please select a vlx file" "D:\\" "vlx" 8)))
  "创建包信息"
  (setq dcl-tmp (vl-filename-mktemp nil nil ".dcl" ))
  (setq dcl_fp (open dcl-tmp "w"))
  (write-line (strcat "createPackage : dialog {label=\"" (_"Create Package for other vlx") "\";" ) dcl_fp)
  (write-line (strcat ":column {:row {:edit_box {label=\""
		      (_"PKG Name")":\";key=\"name\";width=36;fixed_width=true;edit_width=20;alignment=right;}")
	      dcl_fp)
  (write-line (strcat ":text {label=\""(_"English name") "\";key =\"name_desc\";width=50; } }")dcl_fp)
  (write-line (strcat  ":row {: edit_box {label=\""(_"Full Name")":\";" ) dcl_fp)
  (write-line "key=\"fullname\";width=36;fixed_width=true;edit_width=20;alignment=right; }" dcl_fp)
  (write-line (strcat ":text_part {label=\""
		      (_"The name in your local language.")
		      "\";key =\"fullname_desc\";width=50;}}")
	      dcl_fp)
  (write-line (strcat ":row{:button{key=\"vlxbtn\";label=\""
		      "SelectVlx"
		      "\";width=36;fixed_width=true;} :text{key=\"vlxname\";value=\""
		      "Please select a vlx file" "\";width=50;}}")
	      dcl_fp)
  (write-line (strcat ":row{:edit_box{label=\""
		      "vlx Command"
		      "\";key=\"vlxcommand\"; width=36;fixed_width=true;}:text_part{label=\""
		      (_"Please input command to start vlx")
		      "\";key =\"vlx-command-desc\";width=50;}}")
	      dcl_fp)
  (write-line (strcat ":row {:popup_list{label=\""
		      (_"Category")
		      ":\";key=\"category\";width=36;fixed_width=true;edit_width=20;alignment=right;}")
	      dcl_fp)
  (write-line (strcat  ": text_part {	label=\""
		       (_"The package in category.")
		       "\";key =\"category-desc\";width=50;}}")
	      dcl_fp)
  (write-line (strcat ": row {: edit_box {label=\""
		      (_"Description")
		      ":\";key=\"description\";width=95;fixed_width=true;edit_width=80;alignment=right;}}" )
	      dcl_fp)
  (write-line (strcat ": row {: toggle {label=\""
		      (_"Multi Files:include menus,hotkey etc.")
		      "\";key=\"multifiles\";width=95;fixed_width=true;edit_width=80;alignment=right;}}")
	      dcl_fp)
  (write-line "} ok_cancel;}" dcl_fp)
  
  (close dcl_fp)
  (setq dcl_id (load_dialog dcl-tmp))
  (if (not (new_dialog "createPackage" dcl_id))
      (exit))
  (start_list "category")
  (add_list "Uncategorized")
  (mapcar 'add_list @::*pkgs-category*)
  (end_list)
  (action_tile "vlxbtn" "(select-vlx)")
  (action_tile "accept" "(callback-create-package)(done_dialog)")
  (start_dialog)
  (unload_dialog dcl_id)
  (vl-file-delete dcl-tmp)
  (princ)
  )



