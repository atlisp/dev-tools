;;; 生成指定文件的函数说明
;;; 
(in-package :@dev)
(defun @dev::gen-doc (lspfile / pkg-name fp-help)
  "生成函数文档，lspfile :要进行处理的源文件。 pkg-name 函数前缀。"
  (defun para-desc (str-para)
    "生成参数的相关类型说明"
    (setq str-para (strcase str-para T))
    (strcat str-para "  : " 
	    (cond
	      ((wcmatch str-para "numstr*")
	       "数学或字符串")
	      ((wcmatch str-para "separator*")
	       "分隔符")
	      ((wcmatch str-para "blkname*")
	       "块名称")
	      ((wcmatch str-para "blk*")
	       "块图元/对象")
	      ((wcmatch str-para "buzz*")
	       "容差")
	      ((wcmatch str-para "curve*")
	       "曲线")
	      ((wcmatch str-para "prp*")
	       "块特性")
	      ((wcmatch str-para "val*")
	       "值")
	      ((wcmatch str-para "offset*")
	       "偏移量")
	      ((wcmatch str-para "path*")
	       "文件路径")
	      ((wcmatch str-para "ang*")
	       "角度值")
	      ((wcmatch str-para "scale*")
	       "比例值")
	      ((wcmatch str-para "segment*")
	       "线段，两点组成的表，可以表示有向图或无向图的一个边。")
	      ((wcmatch str-para "dxf*")
	       "组码号")
	      ((wcmatch str-para "ents*")
	       "图元列表")
	      ((wcmatch str-para "ent*")
	       "单个图元")
	      ((wcmatch str-para "start*")
	       "起始")
	      ((wcmatch str-para "step*")
	       "步长")
	      ((wcmatch str-para "end*")
	       "结束")
	      ((wcmatch str-para "en*")
	       "单个图元")
	      ((wcmatch str-para "expr*")
	       "表达式，一般指lisp表达式")
	      ((wcmatch str-para "index*")
	       "索引值")
	      ((wcmatch str-para "item")
	       "项或项值")
	      ((wcmatch str-para "key*")
	       "键，关键字")
	      ((wcmatch str-para "label*")
	       "标签")
	      ((wcmatch str-para "desc*")
	       "描述，简述")
	      ((wcmatch str-para "obj*")
	       "activeX 对象")
	      ((wcmatch str-para "ss*")
	       "选择集")
	      ((wcmatch str-para "sel*")
	       "选择集")
	      ((wcmatch str-para "lst*")
	       "列表")
	      ((wcmatch str-para "pts*")
	       "多个坐标点列表")
	      ((wcmatch str-para "pt*")
	       "单个2D/3D坐标点")
	      ((wcmatch str-para "str*")
	       "字符串")
	      ((wcmatch str-para "times*")
	       "整数，常代指次数。")
	      ((wcmatch str-para "int*")
	       "整数")
	      ((wcmatch str-para "real*")
	       "实数")
	      ((wcmatch str-para "fp*")
	       "文件流或文件指针")
	      ((wcmatch str-para "fuzz*")
	       "容差")
	      ((wcmatch str-para "msg*")
	       "提示信息")
	      ((wcmatch str-para "filter*")
	       "过滤dxf组码")
	      ((wcmatch str-para "params*")
	       "参数列表")
	      ((wcmatch str-para "n")
	       "整数")
	      (T "未明确定义"))
	    ))
  (defun para-list (lst / result-str i%)
    (setq result-str "")
    (setq i% 0)
    (if (member '/ lst)
	(setq lst (reverse (cdr (member '/ (reverse lst))))))
    (if lst
	(string:from-lst (mapcar '(lambda (x) (strcase (vl-princ-to-string x) T)) lst) " ")
	"")
    )
  (defun parse-para (lst / result-str i%)
    (setq result-str "")
    (setq i% 0)
    (if (member '/ lst)
	(setq lst (reverse (cdr (member '/ (reverse lst))))))
    (if lst
	(foreach x lst
		 (setq result-str
		       (strcat result-str
			       (itoa (setq i% (1+ i%))) " " (para-desc (strcase (vl-prin1-to-string x) T)) "\n")))
	(setq result-str (strcat result-str (_"None"))))
    result-str
    )
  (defun file:subst-all ( newstr oldstr lspfile / vf zf text)
    "替换文件中的字符串。"
    (if (findfile (strcat lspfile ".to-gendoc.lsp"))
	(vl-file-delete (strcat lspfile ".to-gendoc.lsp")))
    (setq vf (open lspfile "r" ))
    (setq zf (open (strcat lspfile ".to-gendoc.lsp") "w"))
    (while (setq text (read-line vf))
      ;; (print text)
      (write-line (string:subst-all newstr oldstr text) zf)
      );while
    (close vf)
    (close zf)
    )
  (setq pkg-name (last (string:to-lst (car (string:to-lst lspfile ".")) "-")))
  ;; 改 defun 为 defun-q
  (file:subst-all "(defun-q " "(defun " lspfile)
  ;; 加载 lspfile
  (print lspfile)
  (load (strcat lspfile ".to-gendoc.lsp"))
  ;; 从 atoms-family 中得到所有的pkg-name 包定义的函数
  (setq fp-help (open (strcat @::*prefix* "packages\\at-documents\\" pkg-name ".libdoc") "w"))
  (foreach func%
	   (vl-remove
	    ""
	    (mapcar '(lambda (x) (if (eq (car (string:to-lst x ":"))
					 (strcase pkg-name))
				     x ""))
		    (atoms-family 1)))
	   ;; 此处暂为打印到屏幕测试结果，最终生成表文件
	   ;; (函数名 功能 参数 返回值 示例)
	   (format fp-help "(~a ~a ~a ~a ~a ~a)"
		   (vl-prin1-to-string (strcase func% T)) ; 名
		   (vl-prin1-to-string (if (= 'str (type (setq desc (cadr (defun-q-list-ref (read func%))))))
					   desc "")) ; 说明
		   (vl-prin1-to-string (strcat "(" (strcase func% T) " "
					       (para-list (car (defun-q-list-ref (read func%)))) ")")); 用法
		   
		   (vl-prin1-to-string  (parse-para (car (defun-q-list-ref (read func%)))) ); 参数说明
		   (vl-prin1-to-string (if (= 'str (type (setq desc (caddr (defun-q-list-ref (read func%))))))
					   desc "")) ;; 返回值
		   (vl-prin1-to-string  (if (= 'str (type (setq desc (cadddr (defun-q-list-ref (read func%))))))
					    desc "") );; 示例
		   )
	   )
  (close fp-help)
  (format t "---------------------------~%")
  ;; 改 defun-q 为 defun
  )
(defun @dev::read-from-file (lspfile )
  "读取lsp源文件，形成可执行的表。"
  (if (findfile lspfile)
      (read (strcat "(progn " (@::get-file-contents lspfile) ")"))))


